// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FutaCommon",
    defaultLocalization: "vi",
    platforms: [
                .iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FutaCommon",
//            type: .static,
            targets: ["FutaCommon"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", exact: "5.0.1"),
        .package(url: "https://github.com/hackiftekhar/IQKeyboardManager.git", branch: "master"),
        .package(url: "https://github.com/airbnb/lottie-ios.git", exact: "3.4.2"),
        .package(url: "https://github.com/RNCryptor/RNCryptor.git", exact: "5.1.0"),
        .package(url: "https://github.com/littleigloo/Locksmith.git", exact: "1.0.2"),
        .package(url: "https://github.com/simla-tech/Fastis.git", exact: "2.2.0"),
        .package(url: "https://github.com/SDWebImage/SDWebImage.git", exact: "5.9.5"),
        .package(url: "https://gitlab.com/hungnt1486/futa.app.ios.accountManager", branch: "main"),
//        .package(url: "https://gitlab.com/hungnt1486/futa.app.ios.coreapi", branch: "main"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "FutaCommon",
            dependencies: [
                .product(name: "SnapKit", package: "SnapKit"),
                .product(name: "IQKeyboardManagerSwift", package: "IQKeyboardManager"),
                .product(name: "Lottie", package: "lottie-ios"),
                .product(name: "RNCryptor", package: "RNCryptor"),
                .product(name: "Locksmith", package: "Locksmith"),
                .product(name: "Fastis", package: "Fastis"),
                .product(name: "SDWebImage", package: "SDWebImage"),
                .product(name: "FutaAccountManager", package: "futa.app.ios.accountManager"),
//                .product(name: "FutaCoreAPI", package: "futa.app.ios.coreapi"),
            ],
            path:"Sources",
            resources: [.process("Assets")]
        ),
        .testTarget(
            name: "FutaCommonTests",
            dependencies: ["FutaCommon"]),
    ]
)
