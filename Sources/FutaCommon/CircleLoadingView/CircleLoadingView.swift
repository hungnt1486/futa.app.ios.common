//
//  CircleLoadingView.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 23/08/2022.
//

import UIKit
import Lottie

public class CircleLoadingView: BaseLoadingView {
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var animationView: AnimationView!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed("CircleLoadingView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
        configUI()
    }
    
    private func configUI() {
        self.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.cornerRadius = 8
        blurEffectView.backgroundColor = UIColor.clear
        contentView.insertSubview(blurEffectView, at: 0)
    }
    
    public override func show(viewController: UIViewController, userInteraction: Bool = true) {
        super.show(viewController: viewController, userInteraction: userInteraction)
        // Create Animation object
        let jsonName = "circle_animation"
        let animation = Animation.named(jsonName, bundle: Bundle(for: type(of: self)), subdirectory: nil, animationCache: nil)
        animationView.animation = animation
        animationView.loopMode = .loop
        animationView.contentMode = .scaleToFill
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.animationSpeed = 1.5
        animationView.play()
    }
    
    public override func hide() {
        super.hide()
        animationView.stop()
    }
}
