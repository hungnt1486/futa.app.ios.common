//
//  NapasWebViewController.swift
//  FutaPay
//
//  Created by MACOS on 14/11/2022.
//

import UIKit
import WebKit
import RxSwift

public class WebViewBuilder {
    @MainActor public static func build(url: URL, title: String? = nil, accessToken: String? = nil) -> WebViewController {
        let webVC = WebViewController(url: url, title: title, accessToken: accessToken)
        return webVC
    }
}

public class WebViewController: BaseViewController {
    @IBOutlet private weak var webView: WKWebView!
    @IBOutlet private weak var progressView: UIProgressView!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    
    private var mTitle: String?
    private var mURL: URL?
    private var accessToken: String?
    
    private lazy var refreshControl = UIRefreshControl(frame: .zero)
    
    
    init(url: URL, title: String?, accessToken: String?) {
        let bundle = Bundle(for: WebViewController.self)
        super.init(nibName: "WebViewController", bundle: bundle)
        self.mTitle = title
        self.mURL = url
        self.accessToken = accessToken
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        defer {
            clearCache()
        }
        guard self.webView.isLoading else {
            return
        }
        self.webView.stopLoading()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        self.bindEvents()
        self.loadURL()
    }
    
    private func configUI() {
        
        // config webview
        self.webView.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        self.webView.scrollView.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        self.webView.scrollView.addSubview(self.refreshControl)
        
        // config processView
        progressView.progressTintColor = Color.mainTheme
        progressView.trackTintColor = .clear
        progressView.progressViewStyle = .default
        progressView.progress = 0
        progressView.isHidden = true
    }
    
    //MARK: private
    private func bindEvents() {
        self.webView.rx.observeWeakly(Bool.self, #keyPath(WKWebView.isLoading)).bind { [weak self] in
            let isLoading = ($0 ?? false)
            self?.progressView.isHidden = !isLoading
            guard !isLoading, self?.refreshControl.isRefreshing == true else {
                return
            }
            self?.refreshControl.endRefreshing()
        }.disposed(by: disposeBag)
        
        self.webView.rx.observeWeakly(Double.self, #keyPath(WKWebView.estimatedProgress)).bind { [weak self] in
            self?.progressView.progress = Float($0 ?? 0)
        }.disposed(by: disposeBag)
        
        self.refreshControl.rx.controlEvent(.valueChanged).bind { [weak self](_) in
            self?.refreshControl.beginRefreshing()
            self?.loadURL()
        }.disposed(by: disposeBag)
        
        if self.mTitle == nil || self.mTitle?.isEmpty == true {
            self.webView.rx.observeWeakly(String.self, #keyPath(WKWebView.title)).bind { [weak self] in
                self?.titleLabel.text = $0
            }.disposed(by: disposeBag)
        } else {
            titleLabel.text = mTitle?.stripOutHtml()
        }
    }
    
    private func loadURL() {
        guard let url = self.mURL else {return}
        var request = URLRequest(url: url)
        if let accessToken = self.accessToken {
            request.setValue(accessToken, forHTTPHeaderField: "x-access-token")
        }
        self.webView.load(request)
    }
    
    private func clearCache() {
        let dataTypes = Set([WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
        WKWebsiteDataStore.default().removeData(ofTypes: dataTypes, modifiedSince: .distantPast, completionHandler: {})
    }
    
    @IBAction func closeWebView(_ sender: Any) {
        self.dismiss(animated: true)
    }
}




