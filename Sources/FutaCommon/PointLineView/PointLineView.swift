

import UIKit

@IBDesignable
final public class PointLineView: UIView {
    /// Class's public properties.
    lazy var color: UIColor = #colorLiteral(red: 0.6352941176, green: 0.6705882353, blue: 0.7019607843, alpha: 1)

    /// Class's constructors.
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    /// Class's private properties.
    private lazy var path: UIBezierPath = {
        let path = UIBezierPath()

        path.lineCapStyle = .round
        path.lineWidth = 2
        return path
    }()
}

// MARK: - Class's public methods
extension PointLineView {
    public override func draw(_ rect: CGRect) {
        super.draw(rect)

        let p0 = CGPoint(x: self.bounds.midX, y: self.bounds.minY)
        path.move(to: p0)

        let p1 = CGPoint(x: self.bounds.midX, y: self.bounds.maxY)
        path.addLine(to: p1)

        let dashes: [CGFloat] = [0.25, 4.0]
        path.setLineDash(dashes, count: dashes.count, phase: 0.0)

        color.set()
        path.stroke()
    }
}
