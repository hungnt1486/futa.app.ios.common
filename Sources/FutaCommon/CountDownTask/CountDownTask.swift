//
//  CountDownTask.swift
//  Created by Vu Mai Hoang Hai Hung on 9/10/19.


import Foundation
import UIKit

public protocol CountDownTaskDelegate: class {
    func timeCountDown(hour: Int, minutes: Int, second: Int)
    func finishCountDown()
}

public final class CountDownTask {
    private var secondCountDown: Int
    private var tempSecondCountDown = 0
    private var timer: Timer?
    public weak var delegate: CountDownTaskDelegate?
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?

    init() {
        secondCountDown = 0
    }
    
    public init (secondCountDown: Int) {
        self.secondCountDown = secondCountDown
        self.tempSecondCountDown = secondCountDown
    }
    
    public func stopCountDown() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    public func startCountDown() {
        if timer == nil && secondCountDown > 0 {
            backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: { [weak self]  in
                guard let indentifier = self?.backgroundTaskIdentifier else {
                    return
                }
                UIApplication.shared.endBackgroundTask(indentifier)
            })
            
            let time =  calculateTimeCountDown()
            DispatchQueue.main.async {
                self.delegate?.timeCountDown(hour: time.0, minutes: time.1, second: time.2)
            }
            
            
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
                    
                    strongSelf.secondCountDown -= 1
                    if strongSelf.secondCountDown <= -1 {
//                        async {
//                            strongSelf.delegate?.finishCountDown()
//                            strongSelf.stopCountDown()
//                        }
                        DispatchQueue.main.async {
                            strongSelf.secondCountDown = strongSelf.tempSecondCountDown
                            strongSelf.stopCountDown()
                            strongSelf.delegate?.finishCountDown()
                            return
                        }
                        
                    } else {
//                        let hour = strongSelf.secondCountDown / 3600
//                        let minutes = strongSelf.secondCountDown/60 - (hour * 60)
//                        let seconds = strongSelf.secondCountDown - (minutes * 60) - (hour * 3600)
//                        async {
//                            strongSelf.delegate?.timeCountDown(hour: hour, minutes: minutes, second: seconds)
//                        }
                        
                        let time =  strongSelf.calculateTimeCountDown()
                        DispatchQueue.main.async {
                            strongSelf.delegate?.timeCountDown(hour: time.0, minutes: time.1, second: time.2)
                        }
                    }
                })
                RunLoop.current.add(strongSelf.timer!, forMode: .common)
                RunLoop.current.run()
            }
        }
    }
    
    // return (hour, minutes, second)
    private func calculateTimeCountDown() -> (Int, Int, Int) {
        let hour = secondCountDown / 3600
        let minutes = secondCountDown/60 - (hour * 60)
        let seconds = secondCountDown - (minutes * 60) - (hour * 3600)
        return (hour, minutes, seconds)
    }
    
    func startCountDown(fromExpiryTime expiryTime: Int) {
        let date = Date(timeIntervalSince1970: TimeInterval(expiryTime))
        var countdown: DateComponents {
            return Calendar.current.dateComponents([.hour, .minute, .second], from: Date(), to: date)
        }
        
        if timer == nil {
            backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: { [weak self]  in
                guard let indentifier = self?.backgroundTaskIdentifier else {
                    return
                }
                UIApplication.shared.endBackgroundTask(indentifier)
            })
            
            let time =  calculateTimeCountDown()
            DispatchQueue.main.async {
                self.delegate?.timeCountDown(hour: time.0, minutes: time.1, second: time.2)
            }
            
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
                    let hour = countdown.hour!
                    let minutes = countdown.minute!
                    let seconds = countdown.second!
                    if hour <= 0, minutes <= 0, seconds <= 0 {
//                        async {
//                            strongSelf.delegate?.finishCountDown()
//                            strongSelf.stopCountDown()
//                        }
                        DispatchQueue.main.async {
                            strongSelf.secondCountDown = strongSelf.tempSecondCountDown
                            strongSelf.stopCountDown()
                            strongSelf.delegate?.finishCountDown()
                            return
                        }
                        
                    } else {
//                        async {
//                            strongSelf.delegate?.timeCountDown(hour: hour, minutes: minutes, second: seconds)
//                        }
                        DispatchQueue.main.async {
                            strongSelf.delegate?.timeCountDown(hour: hour, minutes: minutes, second: seconds)
                        }
                        
                    }
                })
                RunLoop.current.add(strongSelf.timer!, forMode: .common)
                RunLoop.current.run()
            }
        }
    }
}
