//
//  CarouseCommonView.swift
//  FutaCommon
//
//  Created by Tien Tran on 12/06/2023.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

public class CarouseCommonView: UIView {
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var pageControl: UIPageControl!
    
    private let viewModel: CarouseCommonViewModel = CarouseCommonViewModel(data: [])
    private let xibName: String = "CarouseCommonView"
    private let cellIdentifider = "CommonCarouselSectionPageCell"
    //private let cellWithTitleIdentifider = "CarouselSectionWithTitlePageCell"
    
    private let titleStyleRatio: CGFloat =  184.0 / 343.0
    private let withoutTitleStyleRatio: CGFloat =  183.0 / 375.0
    
    private var autoScrollTimer: Timer?
    private var currentScrollIndex: Int = 0 {
        didSet {
            pageControl.currentPage = currentScrollIndex
        }
    }
    
    let disposeBag = DisposeBag()
    var selectItemCallback: ((String) -> Void)?
    
    public func bindingData(data: [String]) {
        self.viewModel.bindingData(data: data)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        visualize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        visualize()
    }
    
    deinit {
        autoScrollTimer?.invalidate()
    }
    
    //MARK: Config UI Function
    
    public func visualize() {
        initViewWitXibName(xibName)
        initView()
    }
    
    private func initViewWitXibName(_ name: String) {
        Bundle(for: type(of: self)).loadNibNamed(name, owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        
        contentView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalToSuperview()
        }
    }
    
    private func initView() {
        bindingEvents()
        configUI()
    }
    
    //MARK: Private
    
    private func bindingEvents() {
        viewModel
            .showInitDataEvent
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] section in
                guard let self = self else { return }
                self.initAutoScroll()
                self.collectionView.reloadData()
            }).disposed(by: disposeBag)
        
        viewModel
            .showDetailSectionEvent
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] item in
                guard let self = self else { return }
                self.selectItemCallback?(item)
            }).disposed(by: disposeBag)
    }
    
    private  func showLoadDummyView(should: Bool) {
        collectionView.reloadData()
        collectionView.isUserInteractionEnabled = !should
    }
    
    private func configUI() {
        collectionView.register(UINib(nibName: cellIdentifider, bundle: FutaCommonConstant.bundle), forCellWithReuseIdentifier: cellIdentifider)
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func initAutoScroll() {
        if viewModel.items.count == 1 {
            pageControl.isHidden = true
            return
        }
        
        pageControl.isHidden = false
        pageControl.numberOfPages = viewModel.items.count
        autoScrollTimer?.invalidate()
        
        autoScrollTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: {[weak self] timer in
            guard let wSelf = self else { return }
            var next = wSelf.currentScrollIndex + 1
            let count = wSelf.viewModel.items.count
            next = next <= count - 1 ? next : 0
            wSelf.currentScrollIndex = next
            
            if let rect = wSelf.collectionView.layoutAttributesForItem(at: IndexPath(row: next, section: 0))?.frame {
                wSelf.collectionView.scrollRectToVisible(rect, animated: true)
            }
        })
    }
}

extension CarouseCommonView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.isLoadingShimmer ? 1 : viewModel.items.count
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if viewModel.isLoadingShimmer {
            return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * withoutTitleStyleRatio)
        }
        
        let item = viewModel.items[indexPath.row]
        if !item.isEmpty {
            return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * titleStyleRatio)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * withoutTitleStyleRatio)
    }
    
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if viewModel.isLoadingShimmer {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifider,
                                                             for: indexPath) as? CommonCarouselSectionPageCell {
                cell.loadShimmer()
                return cell
            }
        }
        
        let item = viewModel.items[indexPath.row]
        return showCarouselCell(indexPath: indexPath, item: item)
    }
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = viewModel.items[indexPath.row]
        selectItemCallback?(item)
    }
    
    private func showCarouselCell(indexPath: IndexPath, item: String) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifider,
                                                      for: indexPath) as? CommonCarouselSectionPageCell
        if let cell = cell {
            let item = viewModel.items[indexPath.row]
            cell.bindData(item: item)
            return cell
        }
        return UICollectionViewCell()
    }
}

extension CarouseCommonView: UIScrollViewDelegate {
    open func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        autoScrollTimer?.invalidate()
    }
    
    open func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offset = collectionView.contentOffset
        let w = collectionView.bounds.width
        let p = (offset.x / w).rounded(.toNearestOrAwayFromZero)
        currentScrollIndex = Int(p)
        initAutoScroll()
    }
}
