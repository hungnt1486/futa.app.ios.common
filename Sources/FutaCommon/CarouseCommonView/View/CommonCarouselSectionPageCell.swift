//
//  CommonCarouselSectionPageCell.swift
//  FutaCommon
//
//  Created by Tien Tran on 12/06/2023.
//

import UIKit
import SDWebImage

class CommonCarouselSectionPageCell: UICollectionViewCell {
    
    @IBOutlet private weak var bannerImageView: UIImageView!
    
    func bindData(item: String) {
        stopLoadShimmer()
        bannerImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        bannerImageView.sd_setImage(with: URL(string: item))
    }

    func loadShimmer() {
        bannerImageView.startShimmeringAnimation()
    }
    
    private func stopLoadShimmer() {
        bannerImageView.stopShimmeringAnimation()
    }
}
