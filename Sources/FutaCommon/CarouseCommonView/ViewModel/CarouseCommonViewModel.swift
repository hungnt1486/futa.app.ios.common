//
//  CarouseCommonViewModel.swift
//  FutaCommon
//
//  Created by Tien Tran on 12/06/2023.
//

import Foundation
import RxSwift
import Action

class CarouseCommonViewModel: BaseViewModelMVVM {

    private(set) var items: [String] = []

    let showDetailSectionEvent = PublishSubject<String>()
    let showInitDataEvent = PublishSubject<Void>()
    
    var isLoadingShimmer = false
    let hideSectionEvent = PublishSubject<Void>()
    
     init(data: [String]) {
        self.items = data
        super.init()
    }
    
    func bindingData(data: [String]) {
        self.items = data
        showInitDataEvent.onNext(())
    }
}
