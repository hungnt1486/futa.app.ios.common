//
//  KeyboardTopButton.swift
//  FutaCommon
//
//  Created by mac on 31/08/2022.
//

import UIKit

public class KeyboardTopButton: UIView {
    
    @IBOutlet private weak var button: FutaButton!
    @IBOutlet private weak var contentView: UIView!
    private let buttonName: String
    
    public var actionCallBack: (() -> ())?
    
    public init(buttonName: String) {
        self.buttonName = buttonName
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 58))
        commonInit()
    }
    
    /*
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        self.buttonName = ""
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public func setButtonEnable(isEnable: Bool) {
        button.isEnabled = isEnable
    }
    
    //MARK: private
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed("KeyboardTopButton", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
        configUI()
    }
    
    private func configUI() {
        if !buttonName.isEmpty {
            button.setTitle(buttonName, for: .normal)
        }
    }
    
    //MARK: IBAction
    @IBAction private func buttonTouch() {
        actionCallBack?()
    }

}
