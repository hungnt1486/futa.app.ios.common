//
//  CustomCodeViewItem.swift
//  SwiftyCodeView
//

import UIKit


final class CustomCodeItemView: SwiftyCodeItemView {
    
    @IBOutlet private(set) weak var contentH: NSLayoutConstraint!
    @IBOutlet private(set) weak var contentW: NSLayoutConstraint!
    
    override func awakeFromNib() {
        isUserInteractionEnabled = false
        textField.text = ""
        UITextField.appearance().tintColor = UIColor(hexString: "#111111")
        if #available(iOS 12.0, *) {
            textField.textContentType = .oneTimeCode
        }
        
    }
}
