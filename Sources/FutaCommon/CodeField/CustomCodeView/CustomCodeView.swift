//
//  CustomCodeView.swift
//  SwiftyCodeView
//

import Foundation
import UIKit

@IBDesignable
public class CustomCodeView: SwiftyCodeView {
//    private var codeViewItemW: CGFloat = 46
//    private var codeViewItemH: CGFloat = 56
    
    @IBInspectable open var ItemWidth: CGFloat = 32
    @IBInspectable open var ItemHeight: CGFloat = 56
    @IBInspectable open var fontSize: CGFloat = 31
    @IBInspectable open var top: CGFloat = 10
    @IBInspectable open var cursorHeight: CGFloat = 31
   // @IBInspectable open var distanceFromTextField: CGFloat = 20
       
    
    public override func generateItem() -> SwiftyCodeItemView {
        let item = Bundle(for: CustomCodeItemView.self)
            .loadNibNamed("CustomCodeItemView", owner: nil, options: nil)?
            .first as! SwiftyCodeItemView
        if let customCodeItemView = item as? CustomCodeItemView {
            customCodeItemView.contentH.constant = ItemHeight
            customCodeItemView.contentW.constant = ItemWidth
            customCodeItemView.textField.textPadding.top = top
            customCodeItemView.textField.cursorHeight = cursorHeight
          //  customCodeItemView.textField.keyboardDistanceFromTextField = distanceFromTextField
            return customCodeItemView
        }
        
        return item
    }

    public override func resignFirstResponder() -> Bool {
        _ = super.resignFirstResponder()
        return true
        
    }
}
