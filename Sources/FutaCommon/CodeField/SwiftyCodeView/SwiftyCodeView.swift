//
//  SwiftyCodeView.swift
//

import UIKit

public protocol SwiftyCodeViewDelegate: class {
    func codeView(sender: SwiftyCodeView, didFinishInput code: String)
    func deleteBackward(sender: SwiftyCodeView)
}

@IBDesignable
open class SwiftyCodeView: UIControl {
    
    
    @IBInspectable open var length: Int = 4 {
        didSet {
            setupUI()
        }
    }
    
    public weak var delegate: SwiftyCodeViewDelegate?
    
    var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return stackView
    }()
    
    fileprivate var items: [SwiftyCodeItemView] = []
    open var code: String {
        get {
            let items = stackView.arrangedSubviews.map({ $0 as! SwiftyCodeItemView })
            let values = items.map({ $0.textField.text ?? "" })
            return values.joined()
        }
        set {
            if !isStringNumber(newValue) {
                return
            }
            let array = newValue.map(String.init)
            for index in 0..<array.count {
                if index < length {
                    let item = stackView.arrangedSubviews[index] as! SwiftyCodeItemView
                    item.textField.text = array[index]
                }
            }
        }
    }
    
    private func isStringNumber(_ value: String)->Bool {
        return !value.isEmpty && value.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(becomeFirstResponder))
        addGestureRecognizer(tap)
    }
    
    fileprivate func setupUI() {
        stackView.frame = self.bounds
        if stackView.superview == nil {
            addSubview(stackView)
        }
        stackView.arrangedSubviews.forEach { ($0.removeFromSuperview()) }
        
        for index in 0..<length {
            let itemView = generateItem()
            itemView.textField.deleteDelegate = self
            itemView.textField.delegate = self
            itemView.tag = index
            itemView.textField.tag = index
            stackView.addArrangedSubview(itemView)
        }
    }
    
    open func generateItem() -> SwiftyCodeItemView {
        let type = SwiftyCodeItemView.self
        let typeStr = type.description().components(separatedBy: ".").last ?? ""
        let bundle = Bundle(for: type)
        return bundle
            .loadNibNamed(typeStr,
                          owner: nil,
                          options: nil)?
            .last as! SwiftyCodeItemView
    }
    
    override open func becomeFirstResponder() -> Bool {
        let items = stackView.arrangedSubviews
            .map({ $0 as! SwiftyCodeItemView })
        
        let needFocusItem = items.last
        for index in 0..<items.count {
            let item = items[index]
            if let text = item.textField.text {
                if text.isEmpty {
                  //  item.shouldShowBorder(should: true)
                    return item.becomeFirstResponder()
                    
                } else {
                   // item.shouldShowBorder(should: false)
                }
            }
        }
        
        return needFocusItem!.becomeFirstResponder()
        //return (items.filter({ ($0.textField.text ?? "").isEmpty }).first ?? items.last)!.becomeFirstResponder()
    }
    
    override open func resignFirstResponder() -> Bool {
        stackView.arrangedSubviews.forEach({ $0.resignFirstResponder() })
        return true
    }
    
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupUI()
    }
}

extension SwiftyCodeView: UITextFieldDelegate, SwiftyCodeTextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = nil
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
       // textField.placeholder = "•"
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" { //is backspace
            return true
        }
        
        let isNumber = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)
        if isNumber == nil {
            return false
        }
        
        let index = textField.tag
        if !textField.hasText {
            
            let item = stackView.arrangedSubviews[index] as! SwiftyCodeItemView
            item.textField.text = string
         //   item.shouldShowBorder(should: false)
            sendActions(for: .valueChanged)
            if index == length - 1 { //is last textfield
                delegate?.codeView(sender: self, didFinishInput: self.code)
                textField.resignFirstResponder()
                return false
            }
            
            _ = stackView.arrangedSubviews[index + 1].becomeFirstResponder()
        } else {
            let item = stackView.arrangedSubviews[index] as! SwiftyCodeItemView
          //  item.shouldShowBorder(should: false)
            let nextIndex = index + 1
            if nextIndex < length {
                
                _ = stackView.arrangedSubviews[nextIndex].becomeFirstResponder()
                let nextItem = stackView.arrangedSubviews[nextIndex] as! SwiftyCodeItemView
                nextItem.textField.text = string
               // nextItem.shouldShowBorder(should: true)
                if nextIndex == length - 1 { //is last textfield
                    delegate?.codeView(sender: self, didFinishInput: self.code)
                    textField.resignFirstResponder()
                 //   nextItem.shouldShowBorder(should: false)
                    return false
                }
            } else{
                textField.resignFirstResponder()
            }
        }
        
        return false
    }
    
    public func deleteBackward(sender: SwiftyCodeTextField, prevValue: String?) {
        for index in 1..<length {
            let itemView = stackView.arrangedSubviews[index] as! SwiftyCodeItemView
            
            guard itemView.textField.isFirstResponder, (prevValue?.isEmpty ?? true) else {
                continue
            }
            
            let prevItem = stackView.arrangedSubviews[index-1] as! SwiftyCodeItemView
            _ = prevItem.becomeFirstResponder()
            prevItem.textField.text = ""
        }
        delegate?.deleteBackward(sender: self)
        sendActions(for: .valueChanged)
    }
}

extension SwiftyCodeView {
    func resetCode() {
        for index in 0..<length {
            let item = stackView.arrangedSubviews[index] as! SwiftyCodeItemView
            item.textField.resignFirstResponder()
            item.textField.text = nil
        }
    }
    
    func setTextColor(color: UIColor) {
        for index in 0..<length {
            let item = stackView.arrangedSubviews[index] as! SwiftyCodeItemView
            item.textField.textColor = color
        }
    }
}
