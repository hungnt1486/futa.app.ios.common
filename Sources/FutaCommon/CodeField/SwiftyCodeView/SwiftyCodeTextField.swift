//
//  SwiftyCodeTextField.swift
//

import UIKit

public protocol SwiftyCodeTextFieldDelegate: class {
    func deleteBackward(sender: SwiftyCodeTextField, prevValue: String?)
}

open class SwiftyCodeTextField: UITextField {
    
    weak open var deleteDelegate: SwiftyCodeTextFieldDelegate?
    var cursorHeight: CGFloat = 30
    var textPadding = UIEdgeInsets(
            top: 10,
            left: 20,
            bottom: 10,
            right: 20
        )
    
    override open func deleteBackward() {
        let prevValue = text
        super.deleteBackward()        
        deleteDelegate?.deleteBackward(sender: self, prevValue: prevValue)
    }
    
    open override func caretRect(for position: UITextPosition) -> CGRect {
        var rect: CGRect = super.caretRect(for:position)
        rect.origin.y = (rect.size.height - cursorHeight) / 2
        rect = rect.insetBy(dx: 0, dy: (10 - textPadding.top) / 2 )
        rect.size.height = cursorHeight
        
        return rect;
    }
    
    open override func textRect(forBounds bounds: CGRect) -> CGRect {
           let rect = super.textRect(forBounds: bounds)
           return rect.inset(by: textPadding)
       }

    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
           let rect = super.editingRect(forBounds: bounds)
           return rect.inset(by: textPadding)
       }
}

