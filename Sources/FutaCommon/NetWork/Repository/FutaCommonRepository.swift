//
//  FutaCommonRepository.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import Foundation
import RxSwift
import FutaCoreAPI
//import FutaAccountManager

public protocol FutaCommonRepository {
    func getListProvince() -> Observable<[ProvinceModel]>
}

public struct FutaCommonRepositoryImpl: FutaCommonRepository {
    let service: FutaCommonService = FutaCommonServiceImpl()
    
    public func getListProvince() -> Observable<[ProvinceModel]> {
        return service.getListProvince()
            .map { provinceWrapperEntities in
                return provinceWrapperEntities.map { item in
                    return ProvinceModel(id: item.id,
                                         level: item.level,
                                         name: item.name)
                }
            }
    }
}
