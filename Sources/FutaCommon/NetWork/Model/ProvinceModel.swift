//
//  ProvinceModel.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import Foundation

public struct ProvinceModel {
    public let id: Int
    public let level: Int?
    public let name: String
    
    public init(id: Int, level: Int?, name: String) {
        self.id = id
        self.level = level
        self.name = name
    }
}

