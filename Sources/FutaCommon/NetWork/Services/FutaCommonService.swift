//
//  FutaCommonService.swift
//  FutaCoreAPI
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import Foundation
import Alamofire
import RxSwift
import FutaCoreAPI

public protocol FutaCommonService {
    func getListProvince() -> Observable<[ProvinceEntity]>
}

public struct FutaCommonServiceImpl: FutaCommonService {
    public init() {}
    
    public func getListProvince() -> RxSwift.Observable<[ProvinceEntity]> {
        return FutaCommonAPIEndPoint.getListProvince.call()
    }
}

enum FutaCommonAPIEndPoint {
    case getListProvince
}

extension FutaCommonAPIEndPoint: FutaAPICall {
    var interceptor: Alamofire.RequestInterceptor {
        return FutaHttpInterceptor()
    }
    
    var domain: FutaAppEnvironment.FutaDomain {
        switch self {
        case  .getListProvince:
            return .futabus
        }
    }
    
    var path: String {
        switch self {
        case .getListProvince:
            return "/bus-management/routes/public/provinces?size=100"
        }
    }
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case  .getListProvince:
            return .get
        }
    }
    
    var urlParams: [String : Any]? {
        return nil
    }
    
    var paramEncoding: ParameterEncoding {
        switch(self) {
        case .getListProvince:
            return URLEncoding.queryString
        }
    }
    
    var headers: HTTPHeaders {
        var additionalHeader = baseHeader
        let header = HTTPHeader(name: "x-access-token", value: FutaCoreAPIConfig.userToken)
        additionalHeader.add(header)
        return additionalHeader
    }
}
