//
//  ZoneEntities.swift
//  FutaCoreAPI
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import Foundation

public struct ProvinceEntity: Decodable {
    public let id: Int
    public let level: Int
    public let name: String
}
