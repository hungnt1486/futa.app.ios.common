//
//  GetListProvinceUseCase.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import Foundation
import RxSwift

protocol GetListProvinceUseCase {
    func getListProvince() -> Observable<[ProvinceModel]>
}

public struct GetListProvinceUseCaseImpl: GetListProvinceUseCase {
    let repository = FutaCommonRepositoryImpl()
    func getListProvince() -> Observable<[ProvinceModel]> {
        return repository.getListProvince()
    }
}
