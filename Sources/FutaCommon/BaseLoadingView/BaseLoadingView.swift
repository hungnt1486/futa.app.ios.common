//
//  BaseLoadingView.swift
//  FutaCommon
//
//  Created by mac on 18/08/2022.
//

import UIKit

public class BaseLoadingView: UIView {

    public func show(viewController: UIViewController, userInteraction: Bool = true) {
        self.removeFromSuperview()
//        center = viewController.view.center
        center = CGPoint(x: viewController.view.center.x, y: (viewController.view.frame.height - viewController.view.frame.origin.y) / 2)
        viewController.view.addSubview(self)
        viewController.view.isUserInteractionEnabled = userInteraction
    }
    
    public func hide() {
        superview?.isUserInteractionEnabled = true
        self.removeFromSuperview()
    }
}
