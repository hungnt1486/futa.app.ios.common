//
//  FutaButton.swift
//  FUTACustomer
//
//  Created by mac on 17/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class FutaButton: UIButton {
    
    private var tempTitle: String?
    private var activity: UIActivityIndicatorView?
    var shouldEnableUserInteractionWhenLoading = false
    private var enableColor = UIColor(hexString: "#EF5222")
    private var disableColor = UIColor(hexString: "#000000").withAlphaComponent(0.12)
    private var textColor = UIColor.init(hexString: "#ffffff")
    public var font = UIFont.systemFont(ofSize: 16, weight: .medium)
    
    public var hasBorder = false
    public var hasCornerRadius = true
    
    override init(frame: CGRect){
        super.init(frame: frame)
       
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        // fatalError("init(coder:) has not been implemented")
        backgroundColor = enableColor
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    private func setup() {
        self.clipsToBounds = true
        
        if hasCornerRadius {
            self.layer.cornerRadius = self.frame.height / 2
        }
        
        self.titleLabel?.font = font
        self.titleLabel?.textColor = textColor
        self.titleLabel?.textAlignment = .center
        self.setTitleColor(textColor, for: .normal)
        backgroundColor = isEnabled ? enableColor : disableColor
        
    }
    
    public func setBGForButton(hexString: String = "#EF5222") {
        self.enableColor = UIColor.init(hexString: hexString)
    }
    
    public func setTitleColorForButton(hexString: String = "#ffffff") {
        self.textColor = UIColor.init(hexString: hexString)
    }
    
    public func setBorderColorForButton(hexString: String = "#EF5222", width: CGFloat = 1) {
        self.layer.borderWidth = width
        self.layer.borderColor = UIColor.init(hexString: hexString).cgColor
        self.hasBorder = true
    }
    
    override public var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? enableColor.withAlphaComponent(0.5) : enableColor
        }
    }

    override public var isSelected: Bool {
        didSet {
            backgroundColor = isHighlighted ? enableColor.withAlphaComponent(0.5) : enableColor
        }
    }
    
    override public var isEnabled: Bool {
        didSet {
            if self.isEnabled {
                self.backgroundColor = enableColor
                if hasBorder {
                    self.layer.borderColor = enableColor.cgColor
                }
            } else {
                self.backgroundColor = disableColor
                if hasBorder {
                    self.layer.borderColor = disableColor.cgColor
                }
            }
        }
    }
}
