//
//  FutaCommonConfig.swift
//  FutaCommon
//
//  Created by mac on 09/09/2022.
//

import Foundation
import RxSwift

@objcMembers
public class FutaCommonConfig: NSObject {
    public static var isProductMode = true
    
    public static func isLanguageVN() -> Bool {
        let locale = UserDefaults.standard.string(forKey: "app_lang")
        return locale?.elementsEqual("vi") ?? false
    }
    
}
