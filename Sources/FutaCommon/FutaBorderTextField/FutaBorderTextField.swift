//
//  FutaBorderTextField.swift
//  FUTACustomer
//
//  Created by mac on 31/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

fileprivate enum FutaBoderderTextFieldState {
    case clearText // don't animation when refocus
    case focus
}

public enum FutaBorderTextFieldStyle {
    case text
    case button
    case showOnly
}

@IBDesignable public class FutaBorderTextField: UIView {
    
    @IBOutlet public weak var textField: UITextField!
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var borderView: UIView!
    @IBOutlet private weak var clearButton: UIButton!
    @IBOutlet private weak var textFieldTrailLayoutConstraint: NSLayoutConstraint!
    
    private var isShowPassword = false
    private var state: FutaBoderderTextFieldState = .focus
    private var style = FutaBorderTextFieldStyle.text
    private let focusColor = UIColor(hexString: "#F9BAA7")
    private let unFocusColor = UIColor(hexString: "#DDE2E8")
    
    private let errorColor = UIColor(hexString: "#E12424")
    private let trailColor = UIColor(hexString: "#98a1a9")
    private let placeHolderColor = UIColor(hexString: "#A2ABB3")
    
    public var didTextChangeCallback: ((String?) -> ())?
    public var textFieldTapCallback: ((String?) -> ())?
    public var textFieldShouldReturnCallback: (() -> ())?
    public var textFieldDidEndEditingCallback: (() -> ())?
    
    private var isErrorState = false
    private let bundle = Bundle(for: FutaBorderTextField.self)
    private let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
    public var onlyAbcCharater = false

    public override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        commonInit()
    }
    
   public var text: String? {
        get {
            return self.textField.text
        }
        set {
            self.textField.text = newValue
        }
    }
    
   public func setTextfiledStyle(style: FutaBorderTextFieldStyle) {
        self.style = style
        if(style == .text) {
            configforStyleText()
        } else if (style == .button) {
            configForStyleButton()
        } else if(style == .showOnly) {
            configForStyleShowOnly()
        }
    }
    
    
   public func setKeyboardType(keyboardType: UIKeyboardType) {
        textField.keyboardType = keyboardType
    }
    
    func setKeyboardDistanceFromTextField(distance: CGFloat) {
        textField.keyboardDistanceFromTextField = distance
    }
    
    
    public func formatNormal() {
        isErrorState = false
        borderView.borderColor = unFocusColor
    }
    
    public func formatError() {
        isErrorState = true
        borderView.borderColor = errorColor
    }
    
    
    //MARK: @IBInspectable
    
    @IBInspectable
    public var shouldOnlyAbcCharater: Bool {
        set {
            onlyAbcCharater = newValue
        }
        get {
            return onlyAbcCharater
        }
    }
    
    @IBInspectable
    public var isSecure: Bool = false {
        didSet {
            formatSecureType()
        }
    }
    
    @IBInspectable
   public var trailIcon: UIImage? {
           set {
               if (style != .text) {
                   let image = newValue?.withRenderingMode(.alwaysTemplate)
                   clearButton.setImage(image, for: .normal)
               }
           }
           get {
               return clearButton.image(for: .normal)
           }
       }
    
    @IBInspectable
  public var localizedKey: String? {
           set {
               textField.placeholder = newValue?.localized()
           }
           get {
               return textField.placeholder
           }
       }
    
   
    
    //MARK: privarte
    
    
    private func formatSecureType() {
        textField.isSecureTextEntry = isSecure
        clearButton.alpha = 1.0
        
        let image = UIImage(named: "ic_eye_open", in: bundle, compatibleWith: nil)
        clearButton.setImage(image, for: .normal)
        
        clearButton.removeTarget(self, action: #selector(clearButtonTouch), for: .touchUpInside)
        clearButton.addTarget(self, action: #selector(eyeButtonTouch), for: .touchUpInside)
        clearButton.isUserInteractionEnabled  = true
        clearButton.isHidden = false
    }
    
    @objc private func eyeButtonTouch() {
        textField.isSecureTextEntry = isShowPassword
        isShowPassword = !isShowPassword
        
        let iconName = isShowPassword ? "ic_eye_closed" : "ic_eye_open"
        let image = UIImage(named: iconName, in: bundle, compatibleWith: nil)
        clearButton.setImage(image, for: .normal)
        
    }
    
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed("FutaBorderTextField", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
        configUI()
    }
    
    private func configUI() {
        configforStyleText()
        borderView.cornerRadius = 8
        borderView.borderWidth = 1
        borderView.borderColor = unFocusColor
        
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        textField.attributedPlaceholder =
        NSAttributedString(string: "placeholder text", attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor])
        
    }
   
    private func configForStyleShowOnly() {
        textField.isEnabled = false
        textField.textColor = UIColor(hexString: "#637280")
        trailIcon = nil
        textFieldTrailLayoutConstraint.constant = 16
        
        clearButton.removeTarget(self, action: #selector(clearButtonTouch), for: .touchUpInside)
        clearButton.isHidden = true
        
        borderView.backgroundColor = UIColor(hexString: "#F7F7F7")
    }
    
    private func configForStyleButton() {
        
        clearButton.alpha = 1.0
        clearButton.removeTarget(self, action: #selector(clearButtonTouch), for: .touchUpInside)
        clearButton.tintColor = trailColor
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldTap))
        borderView.addGestureRecognizer(tapGesture)
        textField.isEnabled = false
        
        if(trailIcon == nil) {
            textFieldTrailLayoutConstraint.constant = 16
            clearButton.isHidden = true
        } else {
            textFieldTrailLayoutConstraint.constant = 40
            clearButton.isHidden = false
        }
        
    }
    
    @objc private func textFieldTap() {
        textFieldTapCallback?(textField.text)
    }
    
    private func configforStyleText() {
        shouldShowClearButton()
        clearButton.addTarget(self, action: #selector(clearButtonTouch), for: .touchUpInside)
        let image = UIImage(named: "icon_form_cleartext", in: bundle, compatibleWith: nil)
        clearButton.setImage(image, for: .normal)
    }
    
    private func shouldShowBorder(should: Bool) {
        let color = should ? focusColor : unFocusColor
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: [ .curveEaseInOut],
                       animations: { [weak self] in
            guard let self = self  else { return }
            self.borderView.borderColor = color
        }, completion: nil)
    }
    
    private func shouldShowClearButton() {
        var alpha = 0.0
        if let text = textField.text {
            alpha = text.isEmpty ? 0.0 : 1.0
        } else {
            alpha = 0.0
        }
        
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: [ .curveEaseInOut],
                       animations: { [weak self] in
            guard let self = self  else { return }
            
            self.clearButton.alpha = alpha
            self.clearButton.isUserInteractionEnabled = alpha > 0.0
            
        }, completion: { _ in
    
        })
    }
    
    private func shouldhighlightTrailIcon(should: Bool) {
        let color = should ? focusColor : unFocusColor
        let image = clearButton.image(for: .normal)?.withRenderingMode(.alwaysTemplate)
        
        if state == .focus {
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           options: [ .curveEaseInOut],
                           animations: { [weak self] in
                guard let self = self  else { return }
            
                self.clearButton.setImage(image, for: .normal)
                self.clearButton.tintColor = color
                
            }, completion: { _ in
        
            })
        } else {
            self.clearButton.setImage(image, for: .normal)
            self.clearButton.tintColor = color
        }
      
    }
    
    @objc private func clearButtonTouch() {
        textField.text = ""
        didTextChangeCallback?("")
        shouldShowClearButton()
        state = .clearText
        textField.becomeFirstResponder()
    }
    
    @objc private func textFieldDidChange(tf: UITextField) {
        didTextChangeCallback?(tf.text)
        if style == .text, !isSecure {
            shouldShowClearButton()
        }
    }
}

extension FutaBorderTextField: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldShouldReturnCallback?()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if onlyAbcCharater {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else {
            return true
        }
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if isErrorState == false {
            shouldShowBorder(should: true)
        }
        if(style == .button) {
            shouldhighlightTrailIcon(should: true)
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        textFieldDidEndEditingCallback?()
        if isErrorState == false {
            shouldShowBorder(should: false)
        }
        if(style == .button) {
            shouldhighlightTrailIcon(should: false)
        }

    }
    
}
