//
//  BasePopup.swift
//  PEAX
//
//  Created by Hung Hai Hoang Mai Vu on 1/27/21.
//

import UIKit

public enum ShowPopUpTransition {
    case fromTop
    case fromBottom
    case fromCenter
}

open class BasePopupViewController: BaseViewController {
    var viewController = UIViewController()
    private var lastLocation = CGPoint(x: 0, y: 0)
    private var cardView: UIView?
    private var panGesture: UIPanGestureRecognizer?
    
    public var popUpTranstion: ShowPopUpTransition = .fromBottom
    var isFromPresentedVC = false
    var isFullScreen = false
    public var tapBackgroundGesture = UITapGestureRecognizer()
    public var hideWhenTapBackground = false
    
    open override func viewDidLoad() {
        //super.viewDidLoad()
        setupPanGesture()
        setupCornerRadius()
        setupTapBackground()
       
    }
    
    func setupPanGesture() {
        if popUpTranstion == .fromBottom {
            if let contentView = self.view.subviews.first, cardView == nil {
                cardView = contentView
                cardView?.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 14)
                if popUpTranstion == .fromBottom {
                    if panGesture == nil {
                        if isFullScreen {
                            let newPan = UIPanGestureRecognizer(target: self, action: #selector(handleFullScreenPanGesture))
                            panGesture = newPan
                            cardView?.addGestureRecognizer(newPan)
                            cardView?.transform = CGAffineTransform(translationX: 0,
                                                                    y: UIScreen.main.bounds.height / 2)
                        } else {
                            let newPan = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
                            panGesture = newPan
                            cardView?.addGestureRecognizer(newPan)
                        }
                       
                    }
                }
            }
        }
    }
    private func setupTapBackground() {
        if hideWhenTapBackground {
            tapBackgroundGesture = UITapGestureRecognizer(target: self, action: #selector(tapBackground))
            tapBackgroundGesture.delegate = self
            tapBackgroundGesture.cancelsTouchesInView = true
            viewController.view.addGestureRecognizer(tapBackgroundGesture)
        }
    }
    
    private func setupCornerRadius() {
        if popUpTranstion == .fromCenter {
            if let contentView = self.view.subviews.first, cardView == nil {
                cardView = contentView
                cardView?.cornerRadius = 12
            }
        }
    }
    
    @objc open func tapBackground() {
        self.hide()
    }
    
    public func show(animateDuration: CGFloat = 0.3) {
        self.show(animateDuration: animateDuration, nil)
    }
    
    public func show(animateDuration: CGFloat = 0.3, _ completion: (() -> Void)?) {
        let currentWindow = UIApplication.shared.windows.first { $0.isKeyWindow }
        currentWindow?.endEditing(true)
        /* Condition validation: do not present another popup if there is one already */
        let existedView = currentWindow?.subviews.first{ $0.tag == 999 }
        let existedSubView = existedView?.subviews.first{ $0.tag == 888 }
        if let existedSubView = existedSubView,
           let parentVC = existedSubView.getParentViewController(),
           parentVC is Self {
                completion?()
                return
        }
        
        viewController.view.tag = 999
        viewController.view.backgroundColor = .clear
        view.backgroundColor = .clear
        self.view.tag = 888
        viewController.view.addSubview(self.view)
        viewController.addChild(self)
        didMove(toParent: viewController)
        view.frame = viewController.view.frame
        currentWindow?.addSubview(self.viewController.view)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.transitionCrossDissolve, .allowAnimatedContent], animations: { [weak self] in
            self?.viewController.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        })
       
        if popUpTranstion == .fromTop {
            var statusBarHeight: CGFloat = 0
            if isFromPresentedVC {
                if #available(iOS 13.0, *) {
                    statusBarHeight = self.view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
                } else {
                    // Fallback on earlier versions
                    statusBarHeight = 20
                }
            }
            self.view.transform = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
            UIView.transition(with: self.view, duration: animateDuration, options: [.curveEaseInOut]) { [weak self] in
                guard let self = self else { return }
                
                self.view.transform = CGAffineTransform(translationX: 0, y: statusBarHeight)
                
            } completion: { finish in
                completion?()
            }
        } else if popUpTranstion == .fromBottom {
            self.view.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
            UIView.transition(with: self.view, duration: animateDuration, options: [.curveEaseInOut]) { [weak self] in
                guard let self = self else { return }
                
                self.view.transform = CGAffineTransform(translationX: 0, y: 0)
                
            } completion: { finish in
                completion?()
            }
        } else { // .center
            self.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.view.alpha = 0.0;
          
            UIView.animate(withDuration: animateDuration) {
                self.view.alpha = 1.0
                self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            } completion: { (finished) in
                completion?()
            }
        }
    }
    
    @objc public func hide(_ completion: (() -> Void)?) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.transitionCrossDissolve, .allowAnimatedContent], animations: { [weak self] in
            self?.viewController.view.backgroundColor = .clear
        })
        if popUpTranstion == .fromBottom {
            UIView.transition(with: self.view, duration: 0.3, options: [.curveEaseInOut]) { [weak self] in
                guard let self = self else { return }
                self.view.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
                
            } completion: { finish in
                self.viewController.view.removeFromSuperview()
                self.view.removeFromSuperview()
                self.view.transform = CGAffineTransform.identity
                completion?()
            }
        } else if popUpTranstion == .fromTop {
            UIView.transition(with: self.view, duration: 0.5, options: [.curveEaseInOut]) { [weak self] in
                guard let self = self else { return }
                self.view.transform = CGAffineTransform(translationX: 0, y: -self.view.frame.height)
                
            } completion: { finish in
                self.viewController.view.removeFromSuperview()
                self.view.removeFromSuperview()
                self.view.transform = CGAffineTransform.identity
                completion?()
            }
        } else {
        
            UIView.animate(withDuration: 0.25, animations: {
                self.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                self.view.alpha = 0.0;
            }, completion:{ (finished : Bool) in
                if (finished)
                {
                    self.viewController.view.removeFromSuperview()
                    self.view.removeFromSuperview()
                    self.view.transform = CGAffineTransform.identity
                }
                completion?()
            })
        }
    }
    @objc open func hide() {
        self.hide(nil)
    }
    
    @objc func handleFullScreenPanGesture(panGesture: UIPanGestureRecognizer) {
        // get translation
        guard let view = panGesture.view, let cardView = cardView else { return }
        let translation  = panGesture.translation(in: view.superview)
        let spacing = UIScreen.main.bounds.height / 4
        let spacingTop: CGFloat = 45
        /*
        cardView.translatesAutoresizingMaskIntoConstraints = true
        let fullFrame = CGRect(x: 0,
                           y: spacingTop,
                           width: cardView.frame.width,
                           height: UIScreen.main.bounds.height - spacingTop)
         */
        switch panGesture.state {
        case .began:
          //  cardView.frame.size = CGSize(width: cardView.frame.width, height: UIScreen.main.bounds.height - spacingTop)
            self.lastLocation = view.center
            
        case .ended, .cancelled:
            
            let y = lastLocation.y + translation.y
            lastLocation = .zero
            let isDismiss = y > UIScreen.main.bounds.height - spacing
            let isFull = y < spacing
            
            UIView.animate(withDuration: 0.2) {
                if isDismiss {
                    cardView.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height)
                } else if isFull {
                  //  cardView.frame = fullFrame
                    cardView.frame.origin = CGPoint(x: 0, y: spacingTop)
                } else {
//                    let frame = CGRect(x: 0,
//                                       y: UIScreen.main.bounds.height / 2,
//                                       width: cardView.frame.width,
//                                       height: UIScreen.main.bounds.height / 2)
                    cardView.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height / 2)
                   // cardView.frame = frame
                }
                
            } completion: { (finished) in
                if isDismiss {
                    self.viewController.view.removeFromSuperview()
                    self.view.removeFromSuperview()
                    self.view.transform = CGAffineTransform.identity
                    self.hide()
                }
            }
        break
        default:
            var y = lastLocation.y + translation.y
            if y < spacingTop {
                y = spacingTop
            }
            cardView.center = CGPoint(x: cardView.center.x, y: y)
        }
    }
    
    @objc func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        // get translation
        guard let view = panGesture.view, let cardView =  cardView else { return }
        let translation  = panGesture.translation(in: view.superview)
        
        switch panGesture.state {
        case .began:
            self.lastLocation = view.center
        case .ended, .cancelled:
            let y = lastLocation.y + translation.y - cardView.frame.height / 2
            let yOrigin = view.superview!.frame.height - cardView.frame.height * 2 / 3
            lastLocation = .zero
            
            let isDismiss = y > yOrigin
            UIView.animate(withDuration: 0.2) {
                cardView.frame.origin = CGPoint(x: 0, y: isDismiss ? view.superview!.frame.height : view.superview!.frame.height - cardView.frame.height)
            } completion: { (finished) in
                if isDismiss {
                    cardView.frame.origin = CGPoint(x: 0, y: UIScreen.main.bounds.height - cardView.frame.height )
                    self.viewController.view.removeFromSuperview()
                    self.view.removeFromSuperview()
                    self.view.transform = CGAffineTransform.identity
                    self.hide()
                }
            }
        default:
            let y = lastLocation.y + translation.y
            let yOrigin = view.superview!.frame.height - cardView.frame.size.height / 2
            cardView.center = CGPoint(x: cardView.center.x, y: y < yOrigin ? yOrigin : y)
        }
    }
    
    //MARK
    open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let cardView = cardView else { return false }
        let point = gestureRecognizer.location(in: cardView)
        return cardView.bounds.contains(point) ? false : true
    }
}
/*
extension BasePopupViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let cardView = cardView else { return false }
        let point = gestureRecognizer.location(in: cardView)
        return cardView.bounds.contains(point) ? false : true
    }
}
*/
