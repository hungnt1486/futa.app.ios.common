//
//  LoadingInlineView.swift
//  FutaCommon
//
//  Created by mac on 28/08/2022.
//

import UIKit

public class LoadingInLineView: UIView {
    private let activityIndicatorView: UIActivityIndicatorView!
    private var view: UIView?
    
    public init() {
        activityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        //activityIndicatorView.style = .large
        super.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        configUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(view: UIView, isUserInteractionEnabled: Bool) {
        self.view = view
        view.isHidden = true
        self.center = view.center
        view.superview?.addSubview(self)
        view.superview?.getParentViewController()?.view.isUserInteractionEnabled = isUserInteractionEnabled
        activityIndicatorView.startAnimating()
    }
    
    func hide() {
        view?.alpha = 1
        view?.isHidden = false
        view?.superview?.getParentViewController()?.view.isUserInteractionEnabled = true
        activityIndicatorView.stopAnimating()
        self.removeFromSuperview()
    }
    
    public func shouldShowLoading(should: Bool, view: UIView, isUserInteractionEnabled: Bool = true) {
        if should {
            show(view: view, isUserInteractionEnabled: isUserInteractionEnabled)
        } else {
            hide()
        }
    }
    
    //MARK: private
    private func configUI() {
        activityIndicatorView.center = self.center
        addSubview(activityIndicatorView)
    }
}
