//  File name   : UDIDDeviceManager.swift
//
//  Author      : Dung Vu
//  Created date: 1/8/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 Vato. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import Locksmith
import UIKit

@objcMembers
public class UUIDDeviceManager: NSObject {
    public static let instance = UUIDDeviceManager()
    private lazy var _uuidString: String = {
        guard let data = Locksmith.loadDataForUserAccount(userAccount: "mUDIDDeviceManager") else {
            let new = self.generateUUID()
            return new
        }
        
        
        if let uuid = (data["UUID"] as? String) {
            return uuid
        } else {
            let new = self.generateUUID()
            return new
        }
    }()
    
    public var uuidString: String {
        return _uuidString
    }
    
    private func generateUUID() -> String {
        let new = (UIDevice.current.identifierForVendor ?? UUID()).uuidString
        defer {
            try? Locksmith.saveData(data: ["UUID": new], forUserAccount: "mUDIDDeviceManager")
        }
        return new
    }
    
    
    public func getDeviceId() -> String? {
        
        // create a keychain helper instance
        let keychain = KeychainAccess()
        
        // this is the key we'll use to store the uuid in the keychain
        let uuidKey = (Bundle.main.bundleIdentifier ?? "") + "unique_uuid"
        
        // check if we already have a uuid stored, if so return it
        if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey) {
            return uuid
        }
        
        // generate a new id
        guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
            return nil
        }
        
        // store new identifier in keychain
        try? keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)
        
        // return new id
        return newId
    }/// end getDeviceId
}
