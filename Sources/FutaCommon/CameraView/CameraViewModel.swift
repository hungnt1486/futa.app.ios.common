//
//  CameraViewModel.swift
//  FutaCommon
//
//  Created by MACOS on 09/03/2023.
//

import Foundation
import UIKit
import RxSwift

class CameraViewModel: BaseViewModelMVVM {
    let loadImagesEvent = PublishSubject<Void>()
    
    var capturedImages = [UIImage]()
    
    
    override init() {
        super.init()
    }
    
    func viewDidload() {
        loadImagesEvent.onNext(())
    }
    
    func getImageAt(index: Int) -> UIImage {
        return capturedImages[index]
    }
    
    func addPhoto(image: UIImage) {
        capturedImages.insert(image, at: 0)
        loadImagesEvent.onNext(())
    }
    
    func deletePhotoAt(index: Int) {
        capturedImages.remove(at: index)
        loadImagesEvent.onNext(())
    }
}
