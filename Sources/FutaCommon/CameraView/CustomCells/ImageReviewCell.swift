//
//  ImageReviewCell.swift
//  FutaCommon
//
//  Created by MACOS on 09/03/2023.
//

import UIKit

class ImageReviewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    var deletePhotoCallback: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setPhoto(image: UIImage) {
        photoImageView.image = image
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        deletePhotoCallback?()
    }
    
}
