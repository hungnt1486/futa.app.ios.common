//
//  CameraView.swift
//  FutaCommon
//
//  Created by MACOS on 10/03/2023.
//

import UIKit
import RxSwift
import AVFoundation

public class CameraView: UIView {
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var cameraView: UIView!
    @IBOutlet private var numberPictureLabel: UILabel!
    @IBOutlet private var takePictureButton: UIButton!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private weak var heightOfPhotoView: NSLayoutConstraint!
    @IBOutlet private weak var doneView: UIView!
    
    private let disposeBag = DisposeBag()
    
    var max_number_of_photo = 5
    
    public var capturedImagesCallback: (([UIImage]) -> ())?
    public var dissmissCallBack: (() -> ())?
    public var askSettingCallBack: ((UIAlertController) -> ())?
    public var limitedCallBack: ((UIAlertController) -> ())?
    public var reviewPhotoCallBack: ((UIImage, [UIImage]) -> ())?
    
    private let photoOutput = AVCapturePhotoOutput()
    private let captureSession = AVCaptureSession()
    
    private let viewModel = CameraViewModel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configUI()
        bindingEvents()
        viewModel.viewDidload()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configUI()
        bindingEvents()
        viewModel.viewDidload()
        openCamera()
    }
    
    private func configUI() {
        FutaCommonConstant.bundle.loadNibNamed("CameraView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        
        collectionView.register( UINib(nibName: "ImageReviewCell", bundle: FutaCommonConstant.bundle),
                                 forCellWithReuseIdentifier: "ImageReviewCell")
        let edges = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        collectionView.contentInset = edges
    }
    
    private func bindingEvents() {
        
        viewModel
            .loadImagesEvent
            .subscribe(onNext: { [weak self] in
                self?.loadImages()
            }).disposed(by: disposeBag)
        
    }
    
    public func setMaximumPhotos(number: Int) {
        self.max_number_of_photo = number
    }
    
    private func loadImages() {
        takePictureButton.isEnabled = true
        collectionView.reloadData()
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: [ .curveEaseInOut],
                       animations: { [weak self] in
            guard let self = self  else { return }
            self.heightOfPhotoView.constant = (self.viewModel.capturedImages.count > 0) ? 128.0 : 0.0
            self.doneView.isHidden = (self.viewModel.capturedImages.count == 0)
            self.numberPictureLabel.isHidden = (self.viewModel.capturedImages.count == 0)
            self.numberPictureLabel.text = "\(self.viewModel.capturedImages.count)/\(self.max_number_of_photo)"
        }, completion: nil)
    }
    
    public func openCamera() {
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: // the user has already authorized to access the camera.
            self.setupCaptureSession()
            
        case .notDetermined: // the user has not yet asked for camera access.
            AVCaptureDevice.requestAccess(for: .video) { (granted) in
                if granted { // if user has granted to access the camera.
                    DispatchQueue.main.async {
                        self.setupCaptureSession()
                    }
                } else {
                    self.handleDismiss()
                }
            }
            
        case .denied:
            let alert = UIAlertController(title: "unable_to_access_camera".localized(),
                                          message: "how_to_access_camera".localized(),
                                          preferredStyle: UIAlertController.Style.alert)

            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: { _ in
                self.handleDismiss()
            })
            let settingsAction = UIAlertAction(title: "setting".localized(), style: .default, handler: { _ in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl)
                }
            })
            alert.addAction(cancelAction)
            alert.addAction(settingsAction)
            DispatchQueue.main.async {
                self.askSettingCallBack?(alert)
            }
        case .restricted:
            self.handleDismiss()
        default:
            self.handleDismiss()
        }
    }
    
    private func setupCaptureSession() {
        if let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) {
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice)
                if captureSession.canAddInput(input) {
                    captureSession.addInput(input)
                }
            } catch let error {
                print("Failed to set input device with error: \(error)")
            }
            
            if captureSession.canAddOutput(photoOutput) {
                captureSession.addOutput(photoOutput)
            }
            
            let cameraLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            cameraLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: cameraView.frame.size.height)
            cameraLayer.videoGravity = .resizeAspectFill
            self.cameraView.layer.addSublayer(cameraLayer)
            captureSession.startRunning()
            takePictureButton.isHidden = false
        }
    }
    
    private func handleDismiss() {
        DispatchQueue.main.async {
            if self.captureSession.isRunning {
                self.captureSession.stopRunning()
            }
            self.dissmissCallBack?()
        }
    }
    
    
    //MARK: Action
    @IBAction func shootAction(_ sender: Any) {
        if viewModel.capturedImages.count == max_number_of_photo {
            // limitted
            let alert = UIAlertController(title: "notification".localized(),
                                          message: String(format: "limited_photo".localized(), self.max_number_of_photo),
                                          preferredStyle: UIAlertController.Style.alert)

            let okAction = UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil)
            alert.addAction(okAction)
            self.limitedCallBack?(alert)
        }else {
            // take a picture
            takePictureButton.isEnabled = false
            let photoSettings = AVCapturePhotoSettings()
            if let photoPreviewType = photoSettings.availablePreviewPhotoPixelFormatTypes.first {
                photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: photoPreviewType]
                photoOutput.capturePhoto(with: photoSettings, delegate: self)
            }
        }
    }
    
    @IBAction func doneAction(_ sender: Any) {
        capturedImagesCallback?(viewModel.capturedImages)
        self.handleDismiss()
    }
}

extension CameraView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.capturedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        return CGSize(width: 92, height: 92)
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageReviewCell", for: indexPath) as? ImageReviewCell {
            cell.setPhoto(image: viewModel.getImageAt(index: indexPath.row))
            cell.deletePhotoCallback = { [weak self] in
                self?.viewModel.deletePhotoAt(index: indexPath.row)
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = viewModel.getImageAt(index: indexPath.row)
        self.reviewPhotoCallBack?(photo, viewModel.capturedImages)
    }
}

extension CameraView: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation(), let image = UIImage(data: imageData) else { return }
        viewModel.addPhoto(image: image)
    }
}

