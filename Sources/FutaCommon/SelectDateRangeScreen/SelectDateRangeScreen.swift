//
//  SelectDateRange.swift
//  FutaCommon
//
//  Created by mac on 15/12/2022.
//

import UIKit
import Fastis
import SnapKit

public struct FutaDateRange {
    public let fromDate: Date
    public let toDate: Date
}

public class SelectDateRangeScreenBuilder {
    @MainActor public static func build() -> SelectDateRangeScreen {
        let vc = SelectDateRangeScreen()
        vc.isHiddenNavigationBar = true
        return vc
    }
}

public class SelectDateRangeScreen: BaseViewController {
    @IBOutlet private weak var contentView: UIView!
    
    private var currentDateRange: FutaDateRange?
    
    public var selectDateRangeCallback: ((FutaDateRange?) -> Void)?
    
    init() {
        super.init(nibName: "SelectDateRangeScreen", bundle: FutaCommonConstant.bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    
    private func configUI() {
        var customConfig = FastisConfig.default
        customConfig.currentValueView.placeholderTextForRanges = "Chọn ngày"
        
        let fastisController = FastisController(mode: .range, config: customConfig)
      //  let fastisController = FastisController(mode: .range)
        fastisController.title = "Choose range"
        
        fastisController.initialValue = FastisRange(from: Date(), to: Date())
        fastisController.minimumDate = Calendar.current.date(byAdding: .year, value: -10, to: Date())
        fastisController.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
        fastisController.allowToChooseNilDate = true
        fastisController.shortcuts = [.today, .lastWeek, .lastMonth]
        
        fastisController.doneHandler = { [weak self] newValue in
            guard let newValue = newValue else { return }
            self?.currentDateRange = FutaDateRange(fromDate: newValue.fromDate, toDate: newValue.toDate)
        }
        let navVc = UINavigationController(rootViewController: fastisController)
        addChildViewController(controller: navVc, containerView: contentView)
       // fastisController.present(above: self)
        //fastisController.present(above: self)
        
    }

    //MARK: IBAcion
    @IBAction private func buttonCancelTouch() {
        currentDateRange = nil
        selectDateRangeCallback?(currentDateRange)
        if let nav = navigationController {
            nav.popViewController(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
    
    @IBAction private func buttonDoneTouch() {
        selectDateRangeCallback?(currentDateRange)
        if let nav = navigationController {
            nav.popViewController(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
    
  
}
