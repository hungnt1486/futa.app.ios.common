//
//  ImageLabel.swift
//  FutaCityBus
//
//  Created by Vu Mai Hoang Hai Hung on 31/05/2023.
//

import UIKit
public class ImageLabel: UILabel {
 
    public func setText(text: String, color: UIColor, font: UIFont, image: UIImage?, imageRect: CGRect? = nil) {

        let attachment = NSTextAttachment()
        if let imageRect = imageRect {
            attachment.bounds = CGRect(x: 0, y: -5, width: 19, height: 21)
            attachment.bounds = imageRect
        }
        
        attachment.image = image
        let attachmentImage = NSAttributedString(attachment: attachment)
        
        let attributesString = NSMutableAttributedString()
        
        let attribute: [NSAttributedString.Key: Any] = [.foregroundColor: color, NSAttributedString.Key.font: font]
        
        let itemAttribute = NSMutableAttributedString(string: "\n" + text, attributes: attribute)
            
        attributesString.append(attachmentImage)
            
            
        attributesString.append(itemAttribute)
            
        textAlignment = .center
        self.attributedText = attributesString
        self.lineBreakMode = .byWordWrapping
        self.numberOfLines = 0
    }
}
