//
//  LoadingEmptyScreen.swift
//  FutaCommon
//
//  Created by mac on 18/08/2022.
//

import UIKit
import Lottie

public class LoadingAnimation: BaseLoadingView  {
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var animationView: AnimationView!
    
    private static var _instance: LoadingAnimation?
    
    public static var shareInstance: LoadingAnimation {
        get {
            if _instance == nil {
                _instance = LoadingAnimation(frame: CGRect(x: 0, y: 0, width: 220, height: 87))
            }
            return _instance!
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed("LoadingAnimation", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
    }
    
    public override func show(viewController: UIViewController, userInteraction: Bool = true) {
        super.show(viewController: viewController, userInteraction: userInteraction)
        // Create Animation object
        let jsonName = "dots_animation"
        let animation = Animation.named(jsonName, bundle: Bundle(for: type(of: self)), subdirectory: nil, animationCache: nil)
        animationView.animation = animation
        animationView.loopMode = .loop
        animationView.contentMode = .scaleAspectFill
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.animationSpeed = 1.5
        animationView.play()
    }
    
    public override func hide() {
        super.hide()
        animationView.stop()
    }

}
