import Foundation
import UIKit

let TOP_HAS_NOTCH:CGFloat   = 44
let TOP_NO_NOTCH:CGFloat    = 20

// MARK: - String Date Format
let TIME_FORMAT = "kk:mm"
let TIME_FORMAT_02 = "HH:mm"
let DATE_FORMAT = "dd-MM-yyyy"

// MARK: - String Image
let kFutaIconBusStarSelected = "futaIconBusStarSelected"
let kFutaIconBusStarNormal = "futaIconBusStarNormal"
let kEmptyString = ""
let kNoAnswer = "N/A"

// MARK: - System font
let kFont18: CGFloat = 18.0
let kFont16: CGFloat = 16.0
let kFont14: CGFloat = 14.0
let kFont13: CGFloat = 13.0
let kFont15: CGFloat = 15.0
let kFont22: CGFloat = 22.0
let kFont20: CGFloat = 20.0

public struct Color {
    static let mainTheme =  #colorLiteral(red: 0.937254902, green: 0.3215686275, blue: 0.1333333333, alpha: 1) // Primary orange
}

public struct FutaCommonConstant {
//    static let bundle = Bundle(identifier: "FutaCommon.framework")
    static let bundle = Bundle()
}
