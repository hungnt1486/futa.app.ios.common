//
//  LunarDayCell.swift
//  LunarCalendar
//
//  Created by Hoàng Tuấn on 12/7/20.
//  Copyright © 2020 Hoàng Tuấn. All rights reserved.
//

import UIKit

class LunarDayCell: UICollectionViewCell {
    @IBOutlet private weak var solarDayLabel: UILabel!
    @IBOutlet private weak var lunarDayLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    
    private func setupUI(){
        self.solarDayLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        self.lunarDayLabel.font = UIFont.systemFont(ofSize: 11, weight: .regular)
    }
    
    func setBorder(width: CGFloat, color: CGColor) {
        self.contentView.layer.borderWidth = width
        self.contentView.layer.borderColor = color
    }
    
    func setBackgroundColor(color: UIColor) {
        self.contentView.backgroundColor = color
    }
    
    func setupCell(day: SolarAndLunarDate, textSolarDayOfCurrentMonthColor: UIColor, textLunarDayOfCurrentMonthColor: UIColor, currentSolarDay: Int, currentSolarMonth: Int, currentSolarYear: Int) {
        solarDayLabel.text = "\(day.solarDay)"
        lunarDayLabel.text = "\(day.lunarDay)"
        solarDayLabel.isHidden = false
        lunarDayLabel.isHidden = false
        if !day.isDayOfMonth {
            solarDayLabel.isHidden = true
            lunarDayLabel.isHidden = true
        }
        // set color lunar and solar day when it less than current day
        if day.solarYear > currentSolarYear ||
            (day.solarYear == currentSolarYear && day.solarMonth > currentSolarMonth) ||
            (day.solarYear == currentSolarYear && day.solarMonth == currentSolarMonth && day.solarDay >= currentSolarDay) {
            solarDayLabel.textColor = textSolarDayOfCurrentMonthColor
            lunarDayLabel.textColor = textLunarDayOfCurrentMonthColor
        }else {
            solarDayLabel.textColor = UIColor.init(hexString: "#A2ABB3")
            lunarDayLabel.textColor = UIColor.init(hexString: "#A2ABB3")
        }
        
        if day.lunarDay == 1 {
            lunarDayLabel.text = "\(day.lunarDay)/\(day.lunarMonth)"
        }
        
        if day.lunarDay == 1 && day.isDayOfMonth {
            lunarDayLabel.textColor = UIColor.init(hexString: "#00613D")
        }
        
        if day.lunarDay == 1 && !day.isDayOfMonth {
            lunarDayLabel.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.4962542808)
        }
    }
    
    func setupColorCurrentDay(){
        self.solarDayLabel.textColor = UIColor.init(hexString: "#EF5222")
        self.solarDayLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        self.lunarDayLabel.textColor = UIColor.init(hexString: "#EF5222")
    }
    
    func setupColorSelectDay(){
        self.solarDayLabel.textColor = UIColor.init(hexString: "#FFFFFF")
        self.solarDayLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        self.lunarDayLabel.textColor = UIColor.init(hexString: "#FFFFFF").withAlphaComponent(0.7)
    }
    
    func setupColorDeSelectDay(day: SolarAndLunarDate){
        self.solarDayLabel.textColor = UIColor.init(hexString: "111111")
        self.solarDayLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        self.lunarDayLabel.textColor = UIColor.init(hexString: "A2ABB3")
        
        // for case lunar day
        if day.lunarDay == 1 {
            lunarDayLabel.text = "\(day.lunarDay)/\(day.lunarMonth)"
        }
        
        if day.lunarDay == 1 && day.isDayOfMonth {
            lunarDayLabel.textColor = UIColor.init(hexString: "#00613D")
        }
        
        if day.lunarDay == 1 && !day.isDayOfMonth {
            lunarDayLabel.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.4962542808)
        }
        
        // for case current day
        if day.solarDay == Date().calendarDay &&
            day.solarMonth == Date().calendarMonth &&
            day.solarYear == Date().calendarYear &&
            day.isDayOfMonth {
            self.setBackgroundColor(color: UIColor.init(hexString: "#FFF7F5"))
            self.setupColorCurrentDay()
        }
    }
}
