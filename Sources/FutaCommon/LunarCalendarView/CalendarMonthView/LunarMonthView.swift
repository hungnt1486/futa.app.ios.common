//
//  LunarMonthView.swift
//  VNLunarCalendarView
//
//  Created by Hoàng Tuấn on 02/03/2021.
//

import Foundation
import UIKit

class LunarMonthView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var listDay : [SolarAndLunarDate] = [SolarAndLunarDate]() {
        willSet {
            self.collectionView.reloadData()
        }
    }
    
    var currentSolarDay = 0
    var currentSolarMonth = 0
    var currentSolarYear = 0
    
    var dateSelected: SolarAndLunarDate?
    var dateSelect: ((_ date: SolarAndLunarDate) -> Void)?
    var indexOfDateSelected: Int?
    
    var backgroundCurrentDay = UIColor.init(hexString: "#FFF7F5")
    var backgroundDateSelectedColor = UIColor.clear
    var borderColorDay = UIColor.init(hexString: "#D9D9D9")
    var borderWidthDay: CGFloat = 0.5
    var borderColorDaySelected = UIColor.clear
    var borderWidthDaySelected: CGFloat = 0
    var height: CGFloat = 51
    var textSolarDayOfCurrentMonthColor = UIColor.black
    var textLunarDayOfCurrentMonthColor = #colorLiteral(red: 0.9803921569, green: 0.5725490196, blue: 0.2, alpha: 1)
    var saveSelectedIndex: Int = -1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    private func setupView() {
        let date = Date()
        self.currentSolarDay = date.calendarDay
        self.currentSolarMonth = date.calendarMonth
        self.currentSolarYear = date.calendarYear
        Bundle(for: type(of: self)).loadNibNamed("LunarMonthView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        viewDidLoad()
    }
    
    private func viewDidLoad() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "LunarDayCell", bundle: Bundle(for: LunarDayCell.self)), forCellWithReuseIdentifier: "LunarDayCell")
    }
}

extension LunarMonthView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LunarDayCell", for: indexPath) as! LunarDayCell
        cell.contentView.frame = cell.bounds;
        cell.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        cell.setupCell(day: listDay[indexPath.row], textSolarDayOfCurrentMonthColor: textSolarDayOfCurrentMonthColor, textLunarDayOfCurrentMonthColor: textLunarDayOfCurrentMonthColor, currentSolarDay: currentSolarDay, currentSolarMonth: currentSolarMonth, currentSolarYear: currentSolarYear)
        
        // check currentDay
        if listDay[indexPath.row].solarDay == self.currentSolarDay &&
            listDay[indexPath.row].solarMonth == self.currentSolarMonth &&
            listDay[indexPath.row].solarYear == self.currentSolarYear &&
            listDay[indexPath.row].isDayOfMonth {
            cell.contentView.backgroundColor = backgroundCurrentDay
            cell.setupColorCurrentDay()
        }
        
        if listDay[indexPath.row].isDayOfMonth {
            cell.setBorder(width: borderWidthDay, color: borderColorDay.cgColor)
        }else{
            cell.setBorder(width: 0, color: UIColor.clear.cgColor)
        }
        
        if let dateSelected = self.dateSelected {
            if listDay[indexPath.row] == dateSelected {
                cell.setBackgroundColor(color: self.backgroundDateSelectedColor)
                cell.setupColorSelectDay()
                saveSelectedIndex = indexPath.row
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width / 7)
        return CGSize(width: width, height: self.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if listDay[indexPath.row].isDayOfMonth == true &&
         (listDay[indexPath.row].solarYear > currentSolarYear ||
          (listDay[indexPath.row].solarYear == currentSolarYear && listDay[indexPath.row].solarMonth > currentSolarMonth) ||
          (listDay[indexPath.row].solarYear == currentSolarYear && listDay[indexPath.row].solarMonth == currentSolarMonth && listDay[indexPath.row].solarDay >= currentSolarDay)){
            self.dateSelect?(listDay[indexPath.row])
            self.indexOfDateSelected = indexPath.row
            guard let index = indexOfDateSelected else { return }
            for i in 0..<self.listDay.count {
                let indexPath = IndexPath(item: i, section: 0)
                if let cell = collectionView.cellForItem(at: indexPath) as? LunarDayCell {
                    if saveSelectedIndex == i {
                        cell.setupColorDeSelectDay(day: listDay[self.saveSelectedIndex])
                        self.saveSelectedIndex = index
                    }
                    if i == index {
                        cell.setBackgroundColor(color: self.backgroundDateSelectedColor)
                        cell.setupColorSelectDay()
                        if self.saveSelectedIndex == -1 {
                            self.saveSelectedIndex = index
                        }
                    } else {
                        cell.setBackgroundColor(color: .clear)
                        if listDay[indexPath.row].solarDay == self.currentSolarDay &&
                            listDay[indexPath.row].solarMonth == self.currentSolarMonth &&
                            listDay[indexPath.row].solarYear == self.currentSolarYear &&
                            listDay[indexPath.row].isDayOfMonth {
                            cell.contentView.backgroundColor = backgroundCurrentDay
                        }
                    }
                }
            }
        } else {
            return
        }
    }
}
