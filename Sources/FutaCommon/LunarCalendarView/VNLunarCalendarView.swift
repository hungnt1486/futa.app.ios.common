//
//  LunarCalendarView.swift
//  VNLunarCalendarView
//
//  Created by Hoàng Tuấn on 02/03/2021.
//

import Foundation
import UIKit

public protocol VNLunarCalendarViewDelegate: class {
    func dateDidSelect(_ solarDay: Int, _ solarMonth: Int, _ solarYear: Int)
    func monthAndYearIsShowing(_ month: Int, _ year: Int)
    func showOrHidePreNext(isHidePre: Bool, isHideNext: Bool)
}

open class VNLunarCalendarView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet private weak var weekdayView: UIView!
    @IBOutlet private var weekdayLabel: [UILabel]!
    @IBOutlet weak var scrollView: UIScrollView!
    private var listMonth = [[SolarAndLunarDate]]()
    
    open weak var delegate: VNLunarCalendarViewDelegate?
    
    private var currentlySelectedDate: SolarAndLunarDate?
    private var saveCurrentMonthYear: MonthYear = MonthYear.init(month: Date().calendarMonth, year: Date().calendarYear)
    private var currentMonth: MonthYear = MonthYear(month: Date().calendarMonth, year: Date().calendarYear) {
        didSet {
            let currentMonth = self.currentMonth.month
            let currentYear = self.currentMonth.year
            let nextMonth = getNextMonth(currentMonth: currentMonth, currentYear: currentYear)[0]
            let yearOfNextMonth = getNextMonth(currentMonth: currentMonth, currentYear: currentYear)[1]
            
            let nextTwoMonth = getNextTwoMonth(currentMonth: currentMonth, currentYear: currentYear)[0]
            let yearOfNextTwoMonth = getNextTwoMonth(currentMonth: currentMonth, currentYear: currentYear)[1]
            
            let list = [createArrayDayOfMonth(month: currentMonth, year: currentYear),
                        createArrayDayOfMonth(month: nextMonth, year: yearOfNextMonth),
                        createArrayDayOfMonth(month: nextTwoMonth, year: yearOfNextTwoMonth),
                        ]
            listMonth.removeAll()
            listMonth = list
            slides = createSlides()
            
            setupSlideScrollView(slides: slides)
            scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.width, y: 0), animated: false)
        }
    }
    
    var currentIndex = 1
    var slides = [LunarMonthView]()
    var backgroundCurrentDay = UIColor.init(hexString: "#FFF7F5")
    var backgroundDateSelectedColor = UIColor.init(hexString: "#F48664")
    var borderColorDay = UIColor.init(hexString: "#d9d9d9")
    var borderWidthDay: CGFloat = 0.5
    var borderColorDaySelected = UIColor.clear
    var borderWidthDaySelected: CGFloat = 0
    var heightOfDayView: CGFloat = 45
    var textSolarDayOfCurrentMonthColor = UIColor.black
    var textLunarDayOfCurrentMonthColor = UIColor.init(hexString: "#A2ABB3")
    public var maxYear: Int = 0
    private var isFirstTime: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    private func setupView() {
        Bundle(for: type(of: self)).loadNibNamed("VNLunarCalendarView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        viewDidLoad()
    }
    
    private func viewDidLoad() {
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        scrollView.delegate = self
        scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.width, y: 0), animated: false)
    }
    
    public func viewdidAppear(date: Date = Date()){
        if self.saveCurrentMonthYear.month == date.calendarMonth && self.saveCurrentMonthYear.year == date.calendarYear{
            // for case start month
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        }else if self.saveCurrentMonthYear.month - 1 == date.calendarMonth && self.saveCurrentMonthYear.year + 1 == date.calendarYear {
            // for case last month
            scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.width*2, y: 0), animated: false)
        }else{
            scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.width, y: 0), animated: false)
        }
    }
    
    public func setupCalendarView(date: Date = Date()){
        self.currentMonth = MonthYear(month: date.calendarMonth, year: date.calendarYear)
        self.setWeekdayTitle(weekdayTitle: ["mon".localized(), "tue".localized(), "wed".localized(), "thu".localized(), "fri".localized(), "sat".localized(), "sun".localized()])
        self.contentView.backgroundColor = .white
        self.textSolarDayOfCurrentMonthColor = .black
        self.textLunarDayOfCurrentMonthColor = UIColor.init(hexString: "#A2ABB3")
        if self.saveCurrentMonthYear.month == self.currentMonth.month && self.saveCurrentMonthYear.year == self.currentMonth.year {
            // for case start month
            self.delegate?.showOrHidePreNext(isHidePre: true, isHideNext: false)
        }
        else if self.saveCurrentMonthYear.month - 1 == date.calendarMonth && self.saveCurrentMonthYear.year + 1 == date.calendarYear {
            // for case last month
            self.isFirstTime = true
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: true)
            self.currentMonth = MonthYear(month: date.calendarMonth - 2, year: date.calendarYear)
        }
        else {
            self.isFirstTime = true
            if date.calendarMonth == 1 {
                let m = 12
                let y = date.calendarYear - 1
                self.currentMonth = MonthYear(month: m, year: y)
            }else {
                self.currentMonth = MonthYear(month: date.calendarMonth - 1, year: date.calendarYear)
            }
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
        }
        self.delegate?.monthAndYearIsShowing(currentMonth.month, currentMonth.year)
    }
}

// MARK: - Setup Calendar View
extension VNLunarCalendarView {
    
    public func setCurrentlySelectedDate(_ solarDay: Int, _ solarMonth: Int, _ solarYear: Int) {
        let lunarDateArr = self.convertSolar2Lunar(dd: solarDay, mm: solarMonth, yy: solarYear, timeZone: 7)
        guard lunarDateArr.count == 4 else { return }
        self.currentlySelectedDate = SolarAndLunarDate(solarDay: solarDay, solarMonth: solarMonth, solarYear: solarYear, isDayOfMonth: true, lunarDay: lunarDateArr[0], lunarMonth: lunarDateArr[1], lunarYear: lunarDateArr[2])
    }
    
    private func setWeekdayTitle(weekdayTitle: [String]) {
        guard weekdayTitle.count == 7 else { return }
        for i in 0..<weekdayTitle.count {
            weekdayLabel[i].text = weekdayTitle[i]
        }
    }
    
    public func previousPageTouch(){
        let pageIndex = Int(round(scrollView.contentOffset.x / contentView.frame.width))
        var month = currentMonth.month
        var year = currentMonth.year
        if saveCurrentMonthYear.month == month && saveCurrentMonthYear.year == year {
            if month - 1 < 1 {
                month = 12
                year -= 1
            }else{
                month -= 1
            }
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            self.delegate?.monthAndYearIsShowing(month, year)
            self.delegate?.showOrHidePreNext(isHidePre: true, isHideNext: false)
            return
        }
        
        // for case max month year
        var maxMonthYear = self.getMonthYearLater(numberOfMonthAgo: maxYear > 0 ? maxYear*12 - 3 : 0, currentMonth: self.saveCurrentMonthYear.month, currentYear: self.saveCurrentMonthYear.year)
        
        // for case max month year, previous scroll
        if maxMonthYear[0] == month && maxMonthYear[1] == year {
            if pageIndex != currentIndex{
                if maxMonthYear[0] + 1 > 12 {
                    maxMonthYear[0] = 1
                    maxMonthYear[1] = maxMonthYear[1] + 1
                }else{
                    maxMonthYear[0] = maxMonthYear[0] + 1
                }
            }
            let monthAndYearPrevious = self.getPreviousMonth(currentMonth: maxMonthYear[0], currentYear: maxMonthYear[1])
            self.currentMonth = MonthYear(month: monthAndYearPrevious[0], year: monthAndYearPrevious[1])
            self.delegate?.monthAndYearIsShowing(monthAndYearPrevious[0], monthAndYearPrevious[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
            return
        }
        
        // for case first scroll
        var monthAndYearPrevious = self.getPreviousMonth(currentMonth: month, currentYear: year)
        if month == self.saveCurrentMonthYear.month && year == self.saveCurrentMonthYear.year {
            if pageIndex != currentIndex {
                isFirstTime = false
                if monthAndYearPrevious[0] - 1 < 1 {
                    monthAndYearPrevious[0] = 12
                    monthAndYearPrevious[1] -= 1
                }else{
                    monthAndYearPrevious[0] -= 1
                }
            }
        }
        self.currentMonth = MonthYear(month: monthAndYearPrevious[0], year: monthAndYearPrevious[1])
        self.delegate?.monthAndYearIsShowing(monthAndYearPrevious[0], monthAndYearPrevious[1])
        self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
    }
    
    public func nextPageTouch(){
        let pageIndex = Int(round(scrollView.contentOffset.x / contentView.frame.width))
        let month = currentMonth.month
        let year = currentMonth.year
        // for case max month year
        var maxMonthYear = self.getMonthYearLater(numberOfMonthAgo: maxYear > 0 ? maxYear*12 - 3 : 0, currentMonth: self.saveCurrentMonthYear.month, currentYear: self.saveCurrentMonthYear.year)
        
        if maxMonthYear[0] == month && maxMonthYear[1] == year {
            if maxMonthYear[0] + 1 > 12 {
                maxMonthYear[0] = 1
                maxMonthYear[1] += 1
            }else{
                maxMonthYear[0] += 1
            }
            scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.width*2, y: 0), animated: false)
            self.delegate?.monthAndYearIsShowing(maxMonthYear[0], maxMonthYear[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: true)
            return
        }
        
        // for case first scroll
        var monthAndYearNext = self.getNextMonth(currentMonth: month, currentYear: year)
        if month == self.saveCurrentMonthYear.month && year == self.saveCurrentMonthYear.year {
            if pageIndex != currentIndex {
                self.isFirstTime = false
                if monthAndYearNext[0] - 1 < 1 {
                    monthAndYearNext[0] = 12
                    monthAndYearNext[1] -= 1
                }else{
                    monthAndYearNext[0] -= 1
                }
            }
        }
        self.currentMonth = MonthYear(month: monthAndYearNext[0], year: monthAndYearNext[1])
        self.delegate?.monthAndYearIsShowing(monthAndYearNext[0], monthAndYearNext[1])
        self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
    }
}


// MARK: - ScrollView Delegate
extension VNLunarCalendarView: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y != 0 {
            scrollView.contentOffset.y = 0
        }
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageIndex = Int(round(scrollView.contentOffset.x / contentView.frame.width))
        let month = currentMonth.month
        let year = currentMonth.year
        
        var monthNext = self.getNextMonth(currentMonth: month, currentYear: year)
        // for case position 1 next to
        if monthNext[0] - 1 == 0 {
            // special case
            monthNext[0] = 12
            monthNext[1] = monthNext[1] - 1
            self.delegate?.monthAndYearIsShowing(monthNext[0], monthNext[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
        }
        else if monthNext[0] - 1 == self.saveCurrentMonthYear.month {
            self.delegate?.monthAndYearIsShowing(monthNext[0] - 1, monthNext[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
        }
        
        // for case max month year
        var maxMonthYear = self.getMonthYearLater(numberOfMonthAgo: maxYear > 0 ? maxYear*12 - 3 : 0, currentMonth: self.saveCurrentMonthYear.month, currentYear: self.saveCurrentMonthYear.year)
        if maxMonthYear[0] == month && maxMonthYear[1] == year && pageIndex > currentIndex {
            if maxMonthYear[0] + 1 > 12 {
                maxMonthYear[0] = 1
                maxMonthYear[1] = maxMonthYear[1] + 1
            }else{
                maxMonthYear[0] = maxMonthYear[0] + 1
            }
            self.delegate?.monthAndYearIsShowing(maxMonthYear[0], maxMonthYear[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: true)
            return
        }
        // for case max month year, previous scroll
        if maxMonthYear[0] == month && maxMonthYear[1] == year && pageIndex == currentIndex {
            self.delegate?.monthAndYearIsShowing(maxMonthYear[0], maxMonthYear[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
            return
        }
        // end for case max month year
        
        if pageIndex < currentIndex {
            if saveCurrentMonthYear.year == year && saveCurrentMonthYear.month == month {
                if month - 1 == 0 {
                    self.delegate?.monthAndYearIsShowing(12, year - 1)
                }else{
                    self.delegate?.monthAndYearIsShowing(month - 1, year)
                }
                self.delegate?.showOrHidePreNext(isHidePre: true, isHideNext: false)
                return
            }
            let monthAndYearPrevious = self.getPreviousMonth(currentMonth: month, currentYear: year)
            self.currentMonth = MonthYear(month: monthAndYearPrevious[0], year: monthAndYearPrevious[1])
            self.delegate?.monthAndYearIsShowing(monthAndYearPrevious[0], monthAndYearPrevious[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
        } else if pageIndex > currentIndex {
            let monthAndYearNext = self.getNextMonth(currentMonth: month, currentYear: year)
            self.currentMonth = MonthYear(month: monthAndYearNext[0], year: monthAndYearNext[1])
            self.delegate?.monthAndYearIsShowing(monthAndYearNext[0], monthAndYearNext[1])
            self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
        }
        
        if isFirstTime {
            self.isFirstTime = false
            if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x <= 0 {
                // scroll direction right
                let monthNext = self.getNextMonth(currentMonth: month, currentYear: year)
                self.delegate?.monthAndYearIsShowing(monthNext[0], monthNext[1])
                self.delegate?.showOrHidePreNext(isHidePre: false, isHideNext: false)
                
            }
        }
    }
}

// MARK: - Setup Slide ScrollView
extension VNLunarCalendarView {
    func createSlides() -> [LunarMonthView] {
        guard listMonth.count == 3 else { return [LunarMonthView]()}
        var list = [LunarMonthView]()
        for i in 0 ... 2 {
            let slide = LunarMonthView()
            slide.listDay = listMonth[i]
            slide.backgroundCurrentDay = self.backgroundCurrentDay
            slide.backgroundDateSelectedColor = self.backgroundDateSelectedColor
            slide.borderColorDay = self.borderColorDay
            slide.borderWidthDay = self.borderWidthDay
            slide.borderColorDaySelected = self.borderColorDaySelected
            slide.borderWidthDaySelected = self.borderWidthDaySelected
            slide.height = self.heightOfDayView
            slide.textSolarDayOfCurrentMonthColor = self.textSolarDayOfCurrentMonthColor
            slide.textLunarDayOfCurrentMonthColor = self.textLunarDayOfCurrentMonthColor
            
            if let date = self.currentlySelectedDate {
                slide.dateSelected = date
            }
            list.append(slide)
        }
        return list
    }
    
    func setupSlideScrollView(slides : [LunarMonthView]) {
        for subview in self.scrollView.subviews {
            subview.removeFromSuperview()
        }
        scrollView.isPagingEnabled = true
        scrollView.frame = CGRect(x: 0, y: 30, width: self.contentView.frame.width, height: self.contentView.frame.height - 30)
        scrollView.contentSize = CGSize(width: self.contentView.frame.width * CGFloat(slides.count), height: self.contentView.frame.height)
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.scrollView.frame.width * CGFloat(i), y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
            slides[i].dateSelect = { [weak self] date in
                self?.delegate?.dateDidSelect(date.solarDay, date.solarMonth, date.solarYear)
            }
            scrollView.addSubview(slides[i])
        }
    }
}

// MARK: - extension function for solar and lunar calendar
extension VNLunarCalendarView {
    
    func getNextDay(currentDay: Int, currentMonth: Int, currentYear: Int) -> [Int] {
        var day = currentDay + 1
        var month = currentMonth
        var year = currentYear
        if day > getNumberDayOfMonth(month: month, year: year) {
            let monthAndYear = getNextMonth(currentMonth: month, currentYear: year)
            month = monthAndYear[0]
            year = monthAndYear [1]
            day = 1
        }
        return [day, month, year]
    }
        
    func getPreviousDay(currentDay: Int, currentMonth: Int, currentYear: Int) -> [Int] {
        var day = currentDay - 1
        var month = currentMonth
        var year = currentYear
        if day < 1 {
            let monthAndYear = getPreviousMonth(currentMonth: month, currentYear: year)
            month = monthAndYear[0]
            year = monthAndYear [1]
            day = getNumberDayOfMonth(month: month, year: year)
        }
        return [day, month, year]
    }
    
    func isLeapYear(year: Int) -> Bool {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0
    }
    
    func getNumberDayOfMonth(month: Int, year: Int) -> Int {
        switch month {
        case 1, 3, 5, 7, 8, 10, 12:
            return 31
        case 2:
            if isLeapYear(year: year) {
                return 29
            } else {
                return 28
            }
        case 4, 6, 9, 11:
            return 30
        default:
            return 0
        }
    }
    
    func getIndexOfFirstDayInMonth(month: Int, year: Int) -> Int {
        let thu = getThu(dd: 1, mm: month, yy: year)
        let index = CanChi.Thu.firstIndex(of: thu)
        switch index {
        case 0:
            return 6
        default:
            return index! - 1
        }
    }
    
    func getIndexOfLastDayInMonth(month: Int, year: Int) -> Int {
        let thu = getThu(dd: getNumberDayOfMonth(month: month, year: year), mm: month, yy: year)
        let index = CanChi.Thu.firstIndex(of: thu)
        switch index {
        case 0:
            return 6
        default:
            return index! - 1
        }
    }
    
    func getMonthYearAgo(numberOfMonthAgo: Int, currentMonth: Int, currentYear: Int) -> [Int] {
        var month = currentMonth
        var year = currentYear
        for _ in 0 ..< numberOfMonthAgo {
            let previousMonthYear = getPreviousMonth(currentMonth: month, currentYear: year)
            month = previousMonthYear[0]
            year = previousMonthYear[1]
        }
        return [month, year]
    }
    
    func getMonthYearLater(numberOfMonthAgo: Int, currentMonth: Int, currentYear: Int) -> [Int] {
        var month = currentMonth
        var year = currentYear
        for _ in 0 ..< numberOfMonthAgo {
            let previousMonthYear = getNextMonth(currentMonth: month, currentYear: year)
            month = previousMonthYear[0]
            year = previousMonthYear[1]
        }
        return [month, year]
    }
    
    func getPreviousMonth(currentMonth: Int, currentYear: Int) -> [Int] {
        var previousMonth = currentMonth - 1
        var year = currentYear
        if previousMonth < 1 {
            previousMonth = 12
            year -= 1
        }
        return [previousMonth, year]
    }
    
    func getNumberDayOfPreviousMonth(currentMonth: Int, currentYear: Int) -> Int {
        let previousMonthAndYear = getPreviousMonth(currentMonth: currentMonth, currentYear: currentYear)
        let previousMonth = previousMonthAndYear[0]
        let year = previousMonthAndYear[1]
        return getNumberDayOfMonth(month: previousMonth, year: year)
    }
    
    func getNextMonth(currentMonth: Int, currentYear: Int) -> [Int] {
        var nextMonth = currentMonth + 1
        var year = currentYear
        if nextMonth > 12 {
            nextMonth = 1
            year += 1
        }
        return [nextMonth, year]
    }
    
    func getNextTwoMonth(currentMonth: Int, currentYear: Int) -> [Int] {
        var nextMonth = currentMonth + 2
        var year = currentYear
        if nextMonth > 12 {
            nextMonth = 1
            year += 1
        }
        return [nextMonth, year]
    }
    
    func getNumberDayOfNextMonth(currentMonth: Int, currentYear: Int) -> Int {
        let nextMonthYear = getNextMonth(currentMonth: currentMonth, currentYear: currentYear)
        let nextMonth = nextMonthYear[0]
        let year = nextMonthYear[1]
        return getNumberDayOfMonth(month: nextMonth, year: year)
    }
    
    
    func createArrayDayOfMonth(month: Int, year: Int) -> [SolarAndLunarDate] {
        let indexOfFirstDay = getIndexOfFirstDayInMonth(month: month, year: year)
        let indexOfLastDay = getIndexOfLastDayInMonth(month: month, year: year)
        
        let numberDayOfMonth = getNumberDayOfMonth(month: month, year: year)
        var array = [SolarAndLunarDate]()
        for i in 1...numberDayOfMonth {
            let lunar = convertSolar2Lunar(dd: i, mm: month, yy: year, timeZone: 7)
            array.append(SolarAndLunarDate(solarDay: i, solarMonth: month, solarYear: year, isDayOfMonth: true, lunarDay: lunar[0], lunarMonth: lunar[1], lunarYear: lunar[2]))
        }
        if indexOfFirstDay != 0 {
            let numberDayOfPreviousMonth = getNumberDayOfPreviousMonth(currentMonth: month, currentYear: year)
            var previousMonth = month - 1
            var year = year
            if previousMonth < 1 {
                previousMonth = 12
                year -= 1
            }
            for i in 0..<indexOfFirstDay {
                let day = numberDayOfPreviousMonth - i
                let lunar = convertSolar2Lunar(dd: day, mm: previousMonth, yy: year, timeZone: 7)
                array.insert(SolarAndLunarDate(solarDay: day, solarMonth: previousMonth, solarYear: year, isDayOfMonth: false, lunarDay: lunar[0], lunarMonth: lunar[1], lunarYear: lunar[2]), at: 0)
                
            }
        }
        
        if indexOfLastDay != 6 {
            var nextMonth = month + 1
            var year = year
            if nextMonth > 12 {
                nextMonth = 1
                year += 1
            }
            for i in 1...(6-indexOfLastDay) {
                let lunar = convertSolar2Lunar(dd: i, mm: nextMonth, yy: year, timeZone: 7)
                array.append(SolarAndLunarDate(solarDay: i, solarMonth: nextMonth, solarYear: year, isDayOfMonth: false, lunarDay: lunar[0], lunarMonth: lunar[1], lunarYear: lunar[2]))
            }
        }
        
        return array
    }
            /// <summary>
            /// Tìm tên gọi Thứ của ngày
            /// </summary>
            /// <param name="dd"></param>
            /// <param name="mm"></param>
            /// <param name="yy"></param>
            /// <returns></returns>
     func getThu(dd: Int, mm: Int, yy: Int) -> Thu {
         let jd = jdFromDate(dd: dd, mm: mm, yy: yy)
         return CanChi.Thu[((jd + 1) % 7)]
     }
     
     func jdFromDate(dd: Int, mm: Int, yy: Int) -> Int {
         let a: Int = (14 - mm) / 12
         let y: Int = yy + 4800 - a
         let m: Int = mm + 12 * a - 3
         var jd: Int = dd + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045
             if jd < 2299161 {
                 jd = dd + (153 * m + 2) / 5 + 365 * y + y / 4 - 32083
             }
             //jd = jd - 1721425
         return jd
     }
         /**
          * http://www.tondering.dk/claus/calendar.html
          * Section: Is there a formula for calculating the Julian day number?
          * @param jd - the number of days since 1 January 4713 BC (Julian calendar)
          * @return
          */
     func jdToDate(jd: Int) -> [Int] {
         var a: Int = 0
         var b: Int = 0
         var c: Int = 0
         if jd > 2299160 {
             // After 5/10/1582, Gregorian calendar
             a = jd + 32044
             b = (4 * a + 3) / 146097
             c = a - (b * 146097) / 4
         } else {
             b = 0
             c = jd + 32082
         }
         let d: Int = (4 * c + 3) / 1461
         let e: Int = c - (1461 * d) / 4
         let m: Int = (5 * e + 2) / 153
         let day: Int = e - (153 * m + 2) / 5 + 1
         let month: Int = m + 3 - 12 * (m / 10)
         let year: Int = b * 100 + d - 4800 + m / 10
         return [day, month, year]
     }
         /**
          * Solar longitude in degrees
          * Algorithm from: Astronomical Algorithms, by Jean Meeus, 1998
          * @param jdn - number of days since noon UTC on 1 January 4713 BC
          * @return
          */
     func SunLongitude(jdn: Double) -> Double {
             //return CC2K.sunLongitude(jdn)
         return SunLongitudeAA98(jdn: jdn)
     }
     
     func SunLongitudeAA98(jdn: Double) -> Double {
         let T: Double = (jdn - 2451545.0) / 36525 // Time in Julian centuries from 2000-01-01 12:00:00 GMT
         let T2: Double = T * T
         let dr: Double = Double.pi / 180 // degree to radian
         let M: Double = 357.52910 + 35999.05030 * T - 0.0001559 * T2 - 0.00000048 * T * T2 // mean anomaly, degree
         let L0: Double = 280.46645 + 36000.76983 * T + 0.0003032 * T2 // mean longitude, degree
         var DL: Double = (1.914600 - 0.004817 * T - 0.000014 * T2) * sin(dr * M)
         DL = DL + (0.019993 - 0.000101 * T) * sin(dr * 2 * M) + 0.000290 * sin(dr * 3 * M)
         var L: Double = L0 + DL // true longitude, degree
         L = L - Double(360) * Double((INT(L / 360))) // Normalize to (0, 360)
         return L
     }
     
     func INT(_ d: Double) -> Int {
         return Int(floor(d))
     }
     
     func NewMoon(k: Int) -> Double{
         //return CC2K.newMoonTime(k)
         return NewMoonAA98(k: k)
     }
         /**
          * Julian day number of the kth new moon after (or before) the New Moon of 1900-01-01 13:51 GMT.
          * Accuracy: 2 minutes
          * Algorithm from: Astronomical Algorithms, by Jean Meeus, 1998
          * @param k
          * @return the Julian date number (number of days since noon UTC on 1 January 4713 BC) of the New Moon
          */

     func NewMoonAA98(k: Int) -> Double {
         let T: Double = Double(k) / 1236.85 // Time in Julian centuries from 1900 January 0.5
         let T2: Double = T * T
         let T3: Double = T2 * T
         let dr: Double = Double.pi / 180
         let Jd1Temp0 = 29.53058868 * Double(k)
         let Jd1Temp1 = 0.0001178 * T2
         let Jd1Temp2 = 0.000000155 * T3
         var Jd1: Double = 2415020.75933 + Jd1Temp0 + Jd1Temp1 - Jd1Temp2
         let Jd1Temp3 = 132.87 * T
         let Jd1Temp4 = 0.009173 * T2
         let Jd1Temp5 = 166.56 + Jd1Temp3 - Jd1Temp4
         Jd1 = Jd1 + 0.00033 * sin((Jd1Temp5) * dr) // Mean new moon
         let MTemp0 = 29.10535608 * Double(k)
         let MTemp1 = 0.0000333 * T2
         let MTemp2 = 0.0000333 * T2
         let M: Double = 359.2242 + MTemp0 - MTemp1 - MTemp2 // Sun's mean anomaly
         let MprTemp0 = 385.81691806 * Double(k)
         let MprTemp1 = 0.0107306 * T2
         let MprTemp2 = 0.00001236 * T3
         let Mpr: Double = 306.0253 + MprTemp0 + MprTemp1 + MprTemp2 // Moon's mean anomaly
         let FTemp0 = 390.67050646 * Double(k)
         let FTemp1 = 0.0016528 * T2
         let FTemp2 = 0.0016528 * T2
         let F: Double = 21.2964 + FTemp0 - FTemp1 - FTemp2 // Moon's argument of latitude
         var C1: Double = (0.1734 - 0.000393 * T) * sin(M * dr) + 0.0021 * sin(2 * dr * M)
         C1 = C1 - 0.4068 * sin(Mpr * dr) + 0.0161 * sin(dr * 2 * Mpr)
         C1 = C1 - 0.0004 * sin(dr * 3 * Mpr)
         C1 = C1 + 0.0104 * sin(dr * 2 * F) - 0.0051 * sin(dr * (M + Mpr))
         C1 = C1 - 0.0074 * sin(dr * (M - Mpr)) + 0.0004 * sin(dr * (2 * F + M))
         C1 = C1 - 0.0004 * sin(dr * (2 * F - M)) - 0.0006 * sin(dr * (2 * F + Mpr))
         C1 = C1 + 0.0010 * sin(dr * (2 * F - Mpr)) + 0.0005 * sin(dr * (2 * Mpr + M))
         var deltat: Double = 0
         if (T < -11) {
             deltat = 0.001 + 0.000839 * T + 0.0002261 * T2 - 0.00000845 * T3 - 0.000000081 * T * T3
         } else {
             deltat = -0.000278 + 0.000265 * T + 0.000262 * T2
         }
         let JdNew: Double = Jd1 + C1 - deltat
         return JdNew
     }
     
     func getSunLongitude(dayNumber: Int, timeZone: Double) -> Double{
         return SunLongitude(jdn: Double(dayNumber) - 0.5 - timeZone / 24)
     }
      
     func getNewMoonDay(k: Int, timeZone: Double) -> Int {
         let jd: Double = NewMoon(k: k)
         return INT(jd + 0.5 + timeZone / 24)
     }
     
     func getLunarMonth11(yy: Int, timeZone: Double) -> Int{
         let off: Double = Double(jdFromDate(dd: 31, mm: 12, yy: yy)) - 2415021.076998695
         let k: Int = INT(off / 29.530588853)
         var nm: Int = getNewMoonDay(k: k, timeZone: timeZone)
         let sunLong: Int = INT(getSunLongitude(dayNumber: nm, timeZone: timeZone) / 30)
         if sunLong >= 9 {
             nm = getNewMoonDay(k: k - 1, timeZone: timeZone)
         }
         return nm
     }
     
     func getLeapMonthOffset(a11: Int, timeZone: Double) -> Int{
         let a = Double(a11) - 2415021.076998695
         let k: Int = INT(0.5 + a / 29.530588853)
         var last = 0// Month 11 contains point of sun longutide 3*PI/2 (December solstice)
         var i = 1 // We start with the month following lunar month 11
         var arc = INT(getSunLongitude(dayNumber: getNewMoonDay(k: k + i, timeZone: timeZone), timeZone: timeZone) / 30)
         repeat {
             last = arc
             i += 1
             arc = INT(getSunLongitude(dayNumber: getNewMoonDay(k: k + i, timeZone: timeZone), timeZone: timeZone) / 30)
         } while (arc != last && i < 14)
         return i - 1
     }
         /**
          *
          * @param dd
          * @param mm
          * @param yy
          * @param timeZone
          * @return array of [lunarDay, lunarMonth, lunarYear, leapOrNot]
          */
     func convertSolar2Lunar(dd: Int, mm: Int, yy: Int, timeZone: Double) -> [Int] {
         var lunarDay: Int = 0
         var lunarMonth: Int = 0
         var lunarYear: Int = 0
         var lunarLeap: Int = 0
         let dayNumber: Int = jdFromDate(dd: dd, mm: mm, yy: yy)
         let k: Int = INT((Double(dayNumber) - 2415021.076998695) / 29.530588853)
         var monthStart: Int = getNewMoonDay(k: k + 1, timeZone: timeZone)
         if monthStart > dayNumber {
                 monthStart = getNewMoonDay(k: k, timeZone: timeZone)
         }
         var a11: Int = getLunarMonth11(yy: yy, timeZone: timeZone)
         var b11: Int = a11
         if a11 >= monthStart {
             lunarYear = yy
             a11 = getLunarMonth11(yy: yy - 1, timeZone: timeZone)
         } else {
             lunarYear = yy + 1
             b11 = getLunarMonth11(yy: yy + 1, timeZone: timeZone)
         }
         lunarDay = dayNumber - monthStart + 1
         let diff: Int = INT(Double((monthStart - a11) / 29))
         lunarLeap = 0
         lunarMonth = diff + 11
         if (b11 - a11) > 365 {
             let leapMonthDiff: Int = getLeapMonthOffset(a11: a11, timeZone: timeZone)
             if diff >= leapMonthDiff {
                 lunarMonth = diff + 10
                 if diff == leapMonthDiff {
                     lunarLeap = 1
                 }
             }
         }
         
         if lunarMonth > 12 {
             lunarMonth = lunarMonth - 12
         }
         
         if lunarMonth >= 11 && diff < 4 {
             lunarYear -= 1
         }
         return [lunarDay, lunarMonth, lunarYear, lunarLeap]
     }
     
     func convertLunar2Solar(lunarDay: Int, lunarMonth: Int, lunarYear: Int, lunarLeap: Int, timeZone: Double) -> [Int] {
         var a11 = 0
         var b11 = 0
         if lunarMonth < 11 {
             a11 = getLunarMonth11(yy: lunarYear - 1, timeZone: timeZone)
             b11 = getLunarMonth11(yy: lunarYear, timeZone: timeZone)
         } else {
             a11 = getLunarMonth11(yy: lunarYear, timeZone: timeZone)
             b11 = getLunarMonth11(yy: lunarYear + 1, timeZone: timeZone)
         }
         let temp0 = Double(a11) - 2415021.076998695
         let k: Int = INT(0.5 + temp0 / 29.530588853)
         var off = lunarMonth - 11
         if off < 0 {
             off += 12
         }
         
         if (b11 - a11) > 365 {
             let leapOff: Int = getLeapMonthOffset(a11: a11, timeZone: timeZone)
             var leapMonth: Int = leapOff - 2
             if (leapMonth < 0) {
                 leapMonth += 12
             }
             
             if (lunarLeap != 0 && lunarMonth != leapMonth) {
                 return [0, 0, 0 ]
             } else if (lunarLeap != 0 || off >= leapOff) {
                 off += 1
             }
         }
         let monthStart: Int = getNewMoonDay(k: k + off, timeZone: timeZone)
         return jdToDate(jd: monthStart + lunarDay - 1)
     }
}
