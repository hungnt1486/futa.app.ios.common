//
//  CanChi.swift
//  VNLunarCalendarView
//
//  Created by Hoàng Tuấn on 02/03/2021.
//

import Foundation

public struct CanChi {
    public static let Thu: [Thu] = [ .sun, .mon, .tue, .wed, .thu, .fri, .sat ]
}

public enum Thu {
    case mon, tue, wed, thu, fri, sat, sun
    public var string: String {
        switch self {
        case .mon: return "Th 2"
        case .tue: return "Th 3"
        case .wed: return "Th 4"
        case .thu: return "Th 5"
        case .fri: return "Th 6"
        case .sat: return "Th 7"
        case .sun: return "CN"
        }
    }
    
    public var stringFullUpcase: String {
        switch self {
        case .mon: return "THỨ 2"
        case .tue: return "THỨ 3"
        case .wed: return "THỨ 4"
        case .thu: return "THỨ 5"
        case .fri: return "THỨ 6"
        case .sat: return "THỨ 7"
        case .sun: return "CHỦ NHẬT"
        }
    }
    
    public var stringFullNormal: String {
        switch self {
        case .mon: return "monday"
        case .tue: return "tuesday"
        case .wed: return "wednesday"
        case .thu: return "thursday"
        case .fri: return "friday"
        case .sat: return "saturday"
        case .sun: return "sunday"
        }
    }
}
