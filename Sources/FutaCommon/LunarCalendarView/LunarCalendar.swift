//
//  LunarCalendar.swift
//  VNLunarCalendarView
//
//  Created by Hoàng Tuấn on 02/03/2021.
//

import Foundation
import UIKit

public struct MonthYear {
    let month: Int
    let year: Int
    
    init(month: Int, year: Int) {
        self.month = month
        self.year = year
    }
}

public struct SolarAndLunarDate {
    let solarDay: Int
    let solarMonth: Int
    let solarYear: Int
    let isDayOfMonth: Bool
    let lunarDay: Int
    let lunarMonth: Int
    let lunarYear: Int
    
    public init(solarDay: Int, solarMonth: Int, solarYear: Int, isDayOfMonth: Bool, lunarDay: Int, lunarMonth: Int, lunarYear: Int) {
        self.solarDay = solarDay
        self.solarMonth = solarMonth
        self.solarYear = solarYear
        self.isDayOfMonth = isDayOfMonth
        self.lunarDay = lunarDay
        self.lunarMonth = lunarMonth
        self.lunarYear = lunarYear
    }
    
    public static func ==(dateOne: SolarAndLunarDate, dateTow: SolarAndLunarDate) -> Bool{
        return
            dateOne.solarDay == dateTow.solarDay &&
            dateOne.solarMonth == dateTow.solarMonth &&
            dateOne.solarYear == dateTow.solarYear &&
            dateOne.lunarMonth == dateTow.lunarMonth &&
            dateOne.lunarDay == dateTow.lunarDay &&
            dateOne.lunarYear == dateTow.lunarYear &&
            dateOne.isDayOfMonth == true &&
            dateTow.isDayOfMonth == true
    }
}
