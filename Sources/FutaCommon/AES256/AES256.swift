//
//  AES256.swift
//  FUTACustomer
//
//  Created by mac on 10/06/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import CommonCrypto
import RNCryptor
import CryptoKit
import Foundation

public struct AES256 {

    public static let hmacAccesskey = FutaCommonConfig.isProductMode ? "RjxYUOvgw9lIeS3Q1tOK" : "xB9NoNRJScFutaMobile"
    
    public static let encryptionKey: String = "ljhaskdhfkjq43up2"
    
    public static let rasPublicKey = FutaCommonConfig.isProductMode ?  "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtp7PMWQzqRjD9rdQUC8cgq6v6Cfmt8XnBgVjv73y8TdYbv0H8EWI7sPFaa7pHHWQiSrfxMUme5l93d7V8ZS8OJPrYzSOW0D//3BTzXlmF71ljnl5Wx1UOrdzGj96n8urQiXEvyT78LdoupOj41n9WRespMi8TdI0h2zRl7MxWwKEFNKwQ1Bptz2KZ0rKph5NCoeTeeAnCDT1Krz+8a7me60wYTmjD2HRZ6gaGFgkdT4QHFq66WAmoH3MLfNjGpILcd7vLfLinjW4UBuo7tLRQbeC26uS9wzhRYKCZsUppf9VMHD4CY/+Jo9KQBhxnPKTd6JPQRzaFFFLtUf++LMnMQIDAQAB" :
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwR2wuPM5zwjzFtzbf5qW+IrgVd5k/8FQHyl8fdAfwUCA5tMEXEQs7rDExIKe0EYD5WHkl2MlXD9OcrdJvNBA4mX8jwyJj7Br2lZFGCInc/Ik0tkcE+8cO627PJ4VXo5/gTd77OkboYKHsNzOKWgsnAaIOlKJ3nPSo0aB3rB49prVuvBp348X0WsmNV7nB5JeYqdasGi05eSSj0q7EwL9Ltgyu9yL45gL3eKgMxnniewh8yoYCwJI4QfTdt+F/hexoo8chFMV5y9Oxj66Dpy3GZNuVwcPi0RVrYDh4UZ0DKSyXCeS/EFWVo4VoEoiZBnD6HAkY5pmyfaEuygSPeUxYwIDAQAB"
    
    public static func encryptMessage(message: String) -> String {
            let messageData = message.data(using: .utf8)!
            let cipherData = RNCryptor.encrypt(data: messageData, withPassword: encryptionKey)
            return cipherData.base64EncodedString()
        }

    public static func decryptMessage(encryptedMessage: String) -> String? {
        var decryptedString: String?
        let encryptedData = Data.init(base64Encoded: encryptedMessage)!
        do {
            let decryptedData = try RNCryptor.decrypt(data: encryptedData, withPassword: encryptionKey)
            decryptedString = String(data: decryptedData, encoding: .utf8)!
        } catch {}

        return decryptedString
    }
    
    // CommonCrypto HMAC SHA256
    public static func hmacSha256(secretKey: String, message: String) -> String {
        
      let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
      let mac = UnsafeMutablePointer<CChar>.allocate(capacity: digestLength)

      let cSecretKey: [CChar]? = secretKey.cString(using: .utf8)
      let cSecretKeyLength = secretKey.lengthOfBytes(using: .utf8)

      let cMessage: [CChar]? = message.cString(using: .utf8)
      let cMessageLength = message.lengthOfBytes(using: .utf8)

      CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), cSecretKey, cSecretKeyLength, cMessage, cMessageLength, mac)

      let macData = Data(bytes: mac, count: digestLength)

    //  return macData.base64EncodedString()
        
        return macData.toHexString()
    }
    
    public static func rsaEncrypt(string: String, publicKey: String?) -> String? {
            guard let publicKey = publicKey else { return nil }

            let keyString = publicKey.replacingOccurrences(of: "-----BEGIN PUBLIC KEY-----\n", with: "").replacingOccurrences(of: "\n-----END PUBLIC KEY-----", with: "")
            guard let data = Data(base64Encoded: keyString) else {
                return nil
                
            }

            var attributes: CFDictionary {
                return [kSecAttrKeyType         : kSecAttrKeyTypeRSA,
                        kSecAttrKeyClass        : kSecAttrKeyClassPublic,
                        kSecAttrKeySizeInBits   : 2048,
                        kSecReturnPersistentRef : kCFBooleanTrue] as CFDictionary
            }

            var error: Unmanaged<CFError>? = nil
            guard let secKey = SecKeyCreateWithData(data as CFData, attributes, &error) else {
                print(error.debugDescription)
                return nil
            }
            return encrypt(string: string, publicKey: secKey)
        }

    static private func encrypt(string: String, publicKey: SecKey) -> String? {
            let buffer = [UInt8](string.utf8)
            var keySize   = SecKeyGetBlockSize(publicKey)
            var keyBuffer = [UInt8](repeating: 0, count: keySize)

            // Encrypto  should less than key length
            guard SecKeyEncrypt(publicKey, SecPadding.PKCS1, buffer, buffer.count, &keyBuffer, &keySize) == errSecSuccess else {
                return nil
                
            }
            return Data(bytes: keyBuffer, count: keySize).base64EncodedString()
        }
}
