//
//  UIResponder+Extension.swift
//  FutaCommon
//
//  Created by mac on 23/08/2022.
//
import UIKit

public extension UIResponder {
    func getParentViewController() -> UIViewController? {
        if self.next is UIViewController {
            return self.next as? UIViewController
        } else {
            if self.next != nil {
                return (self.next!).getParentViewController()
            }
            else {return nil}
        }
    }
}
