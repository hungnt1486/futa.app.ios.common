//
//  UIViewController+Extensions.swift
//  PEAX
//
//  Created by Vu Mai Hoang Hai Hung
//

import UIKit
import SnapKit

public extension UIViewController {
    static func makeController<T: UIViewController>() -> T {
        return T(nibName: String(describing: T.self), bundle: nil)
    }
    
    static func topMostViewController() -> UIViewController? {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        return self.topViewController(controller: keyWindow?.rootViewController)
    }
    
    static func topViewController(controller: UIViewController?) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    func addChildViewController(controller: UIViewController, containerView: UIView, byConstraints: Bool = false) {
        containerView.addSubview(controller.view)
        addChild(controller)
        controller.didMove(toParent: self)
        
        if byConstraints {
            guard let subView = controller.view else { return }
              subView.snp.makeConstraints { $0.edges.equalToSuperview() }
        } else {
            controller.view.frame = containerView.bounds
        }
    }
    
    func removeAllChildViewController() {
        children.forEach {
            removeChildViewController($0)
        }
    }
    
    /// Remove specific child View Controller from itself except a specific Tag ViewController's View
    func removeAllChildViewControllerExcept(viewByTag tag: Int?) {
        children.forEach {
            if $0.view.tag != tag {
                removeChildViewController($0)
            }
        }
    }
    
    /// Remove specific child View Controller from itself
    func removeChildViewController(_ viewController: UIViewController) {
        viewController.didMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
//    func isModal() -> Bool {
//        return self.presentingViewController?.presentedViewController == self
//            || (self.navigationController != nil && self.navigationController?.presentingViewController?.presentedViewController == self.navigationController)
//            || self.tabBarController?.presentingViewController is UITabBarController
//    }
    
    var isModal: Bool {
        if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 { return false }
        else if presentingViewController != nil { return true }
        else if let navigationController = navigationController, navigationController.presentingViewController?.presentedViewController == navigationController { return true }
        else if let tabBarController = tabBarController, tabBarController.presentingViewController is UITabBarController { return true }
        return false
    }
    
    func isFromViewController<T>(viewControllerClass: T) -> UIViewController? {
        guard let parent = presentingViewController else { return nil }

        if let nav = parent as? UINavigationController {
            for item in nav.viewControllers {
                if item is T {
                    return item
                }
            }
            return nil
        } else {
            if parent is T {
                return parent
            } else {
                return parent.isFromViewController(viewControllerClass: viewControllerClass)
            }
        }
    }
    
    func pushWith(viewController: UIViewController){
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
