//
//  TableView+Extension.swift
//  FutaCommon
//
//  Created by mac on 30/11/2022.
//

import UIKit

public extension UITableView {
   private func loadMoreView() -> UIActivityIndicatorView{
        var activityIndicatorView = UIActivityIndicatorView()
        if self.tableFooterView == nil {
            let indicatorFrame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 80)
            activityIndicatorView = UIActivityIndicatorView(frame: indicatorFrame)
            activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
            
            if #available(iOS 13.0, *) {
                activityIndicatorView.style = .large
            } else {
                // Fallback on earlier versions
                activityIndicatorView.style = .whiteLarge
            }
            
            activityIndicatorView.hidesWhenStopped = true
            self.tableFooterView = activityIndicatorView
            return activityIndicatorView
        }
        else {
            return activityIndicatorView
        }
    }

    func addLoadingMore(_ indexPath:IndexPath, closure: @escaping (() -> Void)){
        if let lastVisibleIndexPath = self.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath && indexPath.row == self.numberOfRows(inSection: self.numberOfSections - 1) - 1 {
                loadMoreView().startAnimating()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    closure()
                }
            }
        }
    }

    func stopLoadingMore() {
        if self.tableFooterView != nil {
            self.loadMoreView().stopAnimating()
        }
        self.tableFooterView = nil
    }
}
