//
//  Double+Extension.swift
//  FutaCommon
//
//  Created by mac on 26/08/2022.
//
import Foundation
public extension Double {
    func toString() -> String {
        return String(format: "%.0f", self)
    }
    
    func dateFormatted(format : String) -> String{
        let date = Date(timeIntervalSince1970: self/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
}

public extension LosslessStringConvertible {
    var string: String {
        let formattedString: String
        if let value = self as? Double {
            if value.truncatingRemainder(dividingBy: 1) == 0 {
                formattedString = String(format: "%.0f", value)
            } else {
                formattedString = .init(self)
            }
        } else {
            formattedString = .init(self)
        }
        return formattedString
    }
}
