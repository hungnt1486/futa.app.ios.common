//
//  Numberic+Extension.swift
//  FutaCommon
//
//  Created by mac on 30/08/2022.
//
import Foundation
public extension Numeric {
    var currency: String {
        return (self as? NSNumber)?.money() ?? ""//.currency(withISO3: "VND", placeSymbolFront: false) ?? ""
    }
    
    var currencyWithoutCode: String {
        return (self as? NSNumber)?.moneyWithoutCurrencySign() ?? ""
    }
}

public extension NSNumber {
    static let formatCurrency = format()
    
    private static func format(currencySign: String = "đ") -> NumberFormatter {
        let format = NumberFormatter()
        format.locale = Locale(identifier: "vi_VN")
        format.numberStyle = .currency
        format.currencyGroupingSeparator = ","
        format.minimumFractionDigits = 0
        format.maximumFractionDigits = 0
        format.currencyCode = "VND"
        format.positiveFormat = "#,###\(currencySign)"
        
        return format
    }
    
    func money() -> String? {
       return NSNumber.formatCurrency.string(from: self)
    }
    
    func moneyWithoutCurrencySign() -> String {
        return NSNumber.format(currencySign: "").string(from: self) ?? ""
    }
}

