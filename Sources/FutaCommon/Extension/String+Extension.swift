//
//  Extension+String.swift
//  FUTACustomer
//
//  Created by adminuser on 16/09/2021.
//  Copyright © 2021 FUTA Group. All rights reserved.
//

import UIKit
import Foundation

public extension String {
    
    /// Check if string is empty
    ///
    /// - Returns: true if empty
    func isTextInputEmpty() -> Bool {
        return (self.isEmpty)
    }
    
    func trim() ->String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func equalsIgnoreCase(_ str:String) -> Bool{
        if caseInsensitiveCompare(str) == ComparisonResult.orderedSame {
            return true
        }
        return false
    }
    
    func widthWithConstrainedHeight(_ height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
    }
    
    func formatToPhoneNumber() -> String {
        
        return replacingOccurrences(of: "(\\d{3})(\\d{4})(\\d+)", with: "$1-$2-$3", options: .regularExpression, range: nil)
    }
    
    // formatting text for salary textField
    func currencyInputFormatting() -> String {
        
        //   var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        
        var amountWithPrefix = self
        
        // remove from String:  ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).intValue
        //  number = NSNumber(value: (double))
        
        /*
         // if first number is 0 or all numbers were deleted
         guard number != 0 as NSNumber else {
         return ""
         }*/
        return double.currency
        //return formatter.string(from: number)!
        
    }
    
    func urlEncoder() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
    }
    
    
    /// Check the string is email or not
    ///
    /// - Returns: true if is email
    func isValidatedEmail() -> Bool {
        let regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return emailPredicate.evaluate(with: self)
    }
    
    /// String to date
    ///
    /// - Parameter format: format of date string
    /// - Returns: Date
    func dateValue(format: String) -> Date {
        //var outputDate: Date!
        var outputDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "vi")
        formatter.timeZone = TimeZone(identifier: "GTM")
        if let date = formatter.date(from: self) {
            outputDate = date
        }
        return outputDate
    }
    
    func dateValueWithTimeZone(format: String, timeZone: TimeZone) -> Date? {
        var outputDate: Date?
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = format
        dateFormat.timeZone = timeZone

        if let date = dateFormat.date(from: self) {
            outputDate = date
        }
        return outputDate
    }
//
//
//
//    /// String to date
//    ///
//    /// - Parameter format: format of date string
//    /// - Returns: Date
//    func dateValueNull(format: String) -> Date? {
//        var outputDate: Date?
//        let formatter = DateFormatter()
//        formatter.dateFormat = format
//        formatter.locale = Locale(identifier: "vi")
//        formatter.timeZone = TimeZone(identifier: "GTM")
//        if let date = formatter.date(from: self) {
//            outputDate = date
//        }
//        return outputDate
//    }
//
//
    
    //
    //
    //
    //    /// String to date
    //    ///
    //    /// - Parameter format: format of date string
    //    /// - Returns: Date
    //    func dateValueNull(format: String) -> Date? {
    //        var outputDate: Date?
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = format
    //        formatter.locale = Locale(identifier: "vi")
    //        formatter.timeZone = TimeZone(identifier: "GTM")
    //        if let date = formatter.date(from: self) {
    //            outputDate = date
    //        }
    //        return outputDate
    //    }
    //
    //
    
    func addingDate(amount: Int) -> String {
        self.dateValue(format: DATE_FORMAT).addingDate(amount: amount)
    }
    
    func addingYear(amount: Int) -> String {
        self.dateValue(format: DATE_FORMAT).addingYear(amount: amount)
    }
    
    
    func addingTime(amount: Int, format: String) -> String {
        self.dateValue(format: format).addingTime(amount: amount, format: format)
    }
    
    //
    //    func toData1(jsonString:String ) -> Data {
    //        let jsonData = jsonString.data(using: String.Encoding.utf8)
    //        return jsonData ?? Data()
    //    }
    
    /// Convert string with format #RRBBGG in to UIColor
    ///
    /// - Returns: UIColor for the string
    func hexColor() -> UIColor {
        let hexint = Int(intFromHexString(self))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        return color
    }
    /// Get int from hex string
    ///
    /// - Parameter hexStr: hex string
    /// - Returns: int
    fileprivate func intFromHexString(_ hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        let scanner: Scanner = Scanner(string: hexStr)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    /// Remove space in string
    ///
    /// - Returns: string without space charaters
    func removeSpaces() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    
    
    //    /// Get string from localize
    //    ///
    //    /// - Parameter key: Key of string
    //    /// - Returns: String
    //    static func localizeFrom(key: String) -> String {
    //
    //        #if DEBUG
    //
    //        if key.hasPrefix( "MSG_") {
    //            return String(format: "%@(%@)", NSLocalizedString(key, comment: kEmptyString), key)
    //        }
    //
    //        #endif
    //
    //        return NSLocalizedString(key, comment: kEmptyString)
    //    }
    
    func replaceNonASCII()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
    
    /// Get substring of string
    ///
    /// - Parameters:
    ///   - start: start index
    ///   - length: length of substring
    /// - Returns: Substring of string
    func subString(start: Int, length: Int = -1) -> String {
        var len = length
        if len == -1 {
            len = self.count - start
        }
        
        let startIndex = self.index(self.startIndex, offsetBy: start)
        let endIndex = self.index(startIndex, offsetBy: len)
        return String(self[startIndex ..< endIndex])
    }
    
    static let regularExpressionOfNotAlphabetAndNumberAndSymbol = "[^a-zA-Z0-9!\"#$%&'()¥*¥+¥-¥.,¥/:;<=>?@¥[¥\\¥]^_`{|}~]"
    
    static let regularExpressionOfNotAlphabetAndNumber = "[^a-zA-Z0-9]+"
    
    static let regularExpressionOfNotNumber: String = "[^0-9]+"
    
    
    
    // Delete Diacritics String
    var trimDiacritics: String {
        return self.folding(options: .diacriticInsensitive, locale: Locale.current)
        
    }
    
    //Delete characters that are not single bytes
    var trimToNumberOnly: String {
        let reg = String.regularExpressionOfNotNumber
        return self.trim(byRegularExpression: reg)
    }
    
    
    
    // Delete characters that are not alphanumeric characters
    var trimToAlphabetAndNumberOnly: String {
        let reg = String.regularExpressionOfNotAlphabetAndNumber
        return self.trim(byRegularExpression: reg)
    }
    
    
    //Delete characters that are not single-digit alphanumeric symbols
    var trimToAlphabetAndNumberAndSymbolOnly: String {
        let reg = String.regularExpressionOfNotAlphabetAndNumberAndSymbol
        return self.trim(byRegularExpression: reg)
        
    }
    
    func trim(byRegularExpression reg: String) -> String {
        return self.replacingOccurrences(of: reg, with: "", options: .regularExpression, range: self.range(of: self))
    }
    
    
    
    
    
    /// String of image name
    ///
    /// - Returns: Image
    func image() -> UIImage {
        if(UIImage.init(named: self)) != nil {
            return UIImage.init(named: self)!
        }
        return UIImage.init()
    }
    
    
    //    func generateAttributedString(with searchTerm: String, targetString: String) -> NSAttributedString? {
    //
    //        let attributedString = NSMutableAttributedString(string: targetString)
    //        do {
    //            let regex = try NSRegularExpression(pattern: searchTerm.trimmingCharacters(in: .whitespacesAndNewlines).folding(options: .diacriticInsensitive, locale: .current), options: .caseInsensitive)
    //            let range = NSRange(location: 0, length: targetString.utf16.count)
    //            for match in regex.matches(in: targetString.folding(options: .diacriticInsensitive, locale: .current), options: .withTransparentBounds, range: range) {
    //                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: kFont15, weight: UIFont.Weight.bold), range: match.range)
    //            }
    //            return attributedString
    //        } catch {
    //            NSLog("Error creating regular expresion: \(error)")
    //            return nil
    //        }
    //    }
    
    /// Set high light word in text
    ///
    /// - Parameters:
    ///   - highlightedWords: text input
    ///
    /// - Returns: return NSMutableAttributedString
    //    func highlightWordsIn(title:
    //                            String , highlightedWords: [String], highlightColor: UIColor? = nil) -> NSMutableAttributedString  {
    //
    //        var newText = NSMutableAttributedString(string: title)
    //
    //        for word in highlightedWords {
    //            newText = apply(string: newText, word: word, highlightColor: highlightColor ?? UIColor.colorPrimary)
    //        }
    //        return newText
    //    }
    //
    
    func apply(string: NSMutableAttributedString, word: String, highlightColor: UIColor) -> NSMutableAttributedString {
        let range = (string.string as NSString).range(of: word)
        return apply(string: string, word: word, range: range, last: range, highlightColor: highlightColor)
    }
    
    func apply(string: NSMutableAttributedString, word: String, range: NSRange, last: NSRange, highlightColor: UIColor) -> NSMutableAttributedString {
        if range.location != NSNotFound {
            
            //
            //            let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: kFont15, weight:  UIFont.Weight.bold),
            //                                                             NSAttributedString.Key.backgroundColor:
            //
            
            
            
            string.addAttribute(NSAttributedString.Key.foregroundColor, value: highlightColor, range: range)
            let start = last.location + last.length
            let end = string.string.count - start
            let stringRange = NSRange(location: start, length: end)
            let newRange = (string.string as NSString).range(of: word, options: [], range: stringRange)
            
            apply(string: string, word: word, range: newRange, last: range, highlightColor: highlightColor)
        }
        return string
    }
    
    
    /// Hide all card number string except 4 final number
    ///
    /// - Parameter cardNumber: string of number need to hide
    /// - Returns: 1234567890 -> ******7890
    
    static func hideCardNumber(cardNumber: String) -> String {
        var cardHidded = cardNumber
        if cardNumber.count >= 5 {
            let cardNumberHideLength = cardNumber.count - 5
            let index = cardNumber.index(cardNumber.startIndex, offsetBy: cardNumberHideLength)
            var replaceString = ""
            for _ in 1...cardNumberHideLength {
                replaceString += "*"
            }
            
            cardHidded.replaceSubrange(cardHidded.startIndex...index, with: replaceString)
        }
        return cardHidded
    }
    
    // uppercase first text
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    
    func indexOf(char: Character) -> Int? {
        return firstIndex(of: char)?.utf16Offset(in: self)
    }
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: .literal, range: nil)
    }
    
    func replace(_ dictionary: [String: String]) -> String{
        var result = String()
        var i = -1
        for (of , with): (String, String)in dictionary{
            i += 1
            if i<1{
                result = self.replacingOccurrences(of: of, with: with)
            }else{
                result = result.replacingOccurrences(of: of, with: with)
            }
        }
        return result
    }
    
    
    func search(_ term: String) -> Bool {
        
        let special_character = "đ"
        let replace_special_character = "d"
        
        let lowercasedSelf = self.lowercased().trimDiacritics.replace(target: special_character, withString: replace_special_character)
        let lowercasedTerm = term.lowercased().trimDiacritics.replace(target: special_character, withString: replace_special_character)
        return lowercasedSelf.localizedCaseInsensitiveContains(lowercasedTerm)
    }
    
    func toFloat() -> Float? {
        let numberFormatter = NumberFormatter()
        return numberFormatter.number(from: self)?.floatValue
    }
    
    func toFloatFromDecimal() -> Float? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.number(from: self)?.floatValue
    }
    
    func toDouble() -> Double? {
        let numberFormatter = NumberFormatter()
        var newString = self.replacingOccurrences(of: ",", with: "")
        newString = newString.replacingOccurrences(of: ".", with: "")
        newString = newString.replacingOccurrences(of: "đ", with: "")
        return numberFormatter.number(from: newString)?.doubleValue
    }
    
    func toInt() -> Int? {
        let numberFormatter = NumberFormatter()
        var newString = self.replacingOccurrences(of: ",", with: "")
        newString = newString.replacingOccurrences(of: ".", with: "")
        newString = newString.replacingOccurrences(of: "đ", with: "")
        return numberFormatter.number(from: newString)?.intValue
    }
    
    func stripOutHtml() -> String? {
        do {
            guard let data = self.data(using: .unicode) else {
                return nil
            }
            let attributed = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return attributed.string
        } catch {
            return nil
        }
    }
    
    func htmlAttributedString() -> NSAttributedString? {
        let htmlTemplate = """
        <!doctype html>
        <html>
          <head>
            <style>
              body {
                font-family: -apple-system;
                font-size: 13px;
                font-style: normal;
                font-weight: 400;
                line-height: 16px;
                color: #637280;
              }
              b {
                font-family: -apple-system;
                font-size: 13px;
                font-style: normal;
                font-weight: 500;
                color: #4A4A4A;
              }
            </style>
          </head>
          <body>
            \(self)
          </body>
        </html>
        """
        
        guard let data = htmlTemplate.data(using: .utf8) else {
            return nil
        }
        
        guard let attributedString = try? NSAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil
        ) else {
            return nil
        }
        
        return attributedString
    }
    
    func generateQRCodeForHome() -> UIImage? {
        let data = self.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("H", forKey: "inputCorrectionLevel")
            let transform = CGAffineTransform(scaleX: 4, y: 4)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
}


extension NSMutableAttributedString {
    
    //    https:// stackoverflow.com/questions/28496093/making-text-bold-using-attributed-string-in-swift
    
    var fontSize:CGFloat { return 14 }
    var boldFont:UIFont { return UIFont.systemFont(ofSize: fontSize, weight:  UIFont.Weight.bold) }
    var normalFont:UIFont { return UIFont.systemFont(ofSize: fontSize, weight:  UIFont.Weight.regular) }
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}

// Calculate hash
extension String {
    var asciiArray: [Int32] {
        return unicodeScalars.filter { $0.isASCII }.map { Int32($0.value) }
    }
    func javaHash() -> Int32 {
        let codes = asciiArray
        let result = codes.reduce(0) { (result, next) -> Int32 in
            let r = result.multipliedReportingOverflow(by: 31)
            let f = r.partialValue.addingReportingOverflow(next)
            return f.partialValue
        }
        return abs(result)
    }
}

extension String {
    subscript(range: ClosedRange<Int>) -> String {
        let range = NSMakeRange(range.lowerBound, range.upperBound - range.lowerBound)
        let r = (self as NSString).substring(with: range)
        return r
    }
    
    static func format(number n: UInt32, base: UInt32) -> String {
        struct Alphabet {
            static let string = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        }
        let s = Alphabet.string
        precondition(base < s.count, "Not enough characters")
        let substring = s[0...Int(base)]
        return self.format(number: n, stringAlphabet: substring)
    }
    
    fileprivate static func format(number n: UInt32, stringAlphabet: String) -> String {
        let lenght = UInt32(stringAlphabet.count)
        precondition(lenght > 0, "Recheck")
        
        let result: String
        if n < lenght {
            let idx = Int(n)
            result = stringAlphabet[idx...idx + 1]
        } else {
            let first = self.format(number: n / lenght, stringAlphabet: stringAlphabet)
            let idx = Int(n % lenght)
            let second = stringAlphabet[idx...idx + 1]
            result = String(format: "%@%@", first, second)
        }
        return result
    }
}

extension StringProtocol {
    subscript(offset: Int) -> Character { self[index(startIndex, offsetBy: offset)] }
    subscript(range: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: PartialRangeFrom<Int>) -> SubSequence { self[index(startIndex, offsetBy: range.lowerBound)...] }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence { self[...index(startIndex, offsetBy: range.upperBound)] }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence { self[..<index(startIndex, offsetBy: range.upperBound)] }
}

public extension NSMutableAttributedString {

    public func trimmedAttributedString(set: CharacterSet) -> NSMutableAttributedString {

        let invertedSet = set.inverted

        var range = (string as NSString).rangeOfCharacter(from: invertedSet)
        let loc = range.length > 0 ? range.location : 0

        range = (string as NSString).rangeOfCharacter(
                            from: invertedSet, options: .backwards)
        let len = (range.length > 0 ? NSMaxRange(range) : string.count) - loc

        let r = self.attributedSubstring(from: NSMakeRange(loc, len))
        return NSMutableAttributedString(attributedString: r)
    }
}
