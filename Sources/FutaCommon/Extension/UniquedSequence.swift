//
//  Unique.swift
//  FutaCommon
//
//  Created by Tien Tran on 04/01/2023.
//

/// A sequence wrapper that leaves out duplicate elements of a base sequence.
public struct UniquedSequence<Base: Sequence, Subject: Hashable> {
  /// The base collection.
  @usableFromInline
  internal let base: Base
  
  /// The projection function.
  @usableFromInline
  internal let projection: (Base.Element) -> Subject
  
  @usableFromInline
  internal init(base: Base, projection: @escaping (Base.Element) -> Subject) {
    self.base = base
    self.projection = projection
  }
}

extension UniquedSequence: Sequence {
  /// The iterator for a `UniquedSequence` instance.
  public struct Iterator: IteratorProtocol {
    @usableFromInline
    internal var base: Base.Iterator
    
    @usableFromInline
    internal let projection: (Base.Element) -> Subject
    
    @usableFromInline
    internal var seen: Set<Subject> = []
    
    @usableFromInline
    internal init(
      base: Base.Iterator,
      projection: @escaping (Base.Element) -> Subject
    ) {
      self.base = base
      self.projection = projection
    }
    
    @inlinable
    public mutating func next() -> Base.Element? {
      while let element = base.next() {
        if seen.insert(projection(element)).inserted {
          return element
        }
      }
      return nil
    }
  }
  
  @inlinable
  public func makeIterator() -> Iterator {
    Iterator(base: base.makeIterator(), projection: projection)
  }
}


extension Sequence {
  /// Returns an array with the unique elements of this sequence (as determined
  /// by the given projection), in the order of the first occurrence of each
  /// unique element.
  ///
  /// This example finds the elements of the `animals` array with unique
  /// first characters:
  ///
  ///     let animals = ["dog", "pig", "cat", "ox", "cow", "owl"]
  ///     let uniqued = animals.uniqued(on: { $0.first })
  ///     print(uniqued)
  ///     // Prints '["dog", "pig", "cat", "ox"]'
  ///
  /// - Parameter projection: A closure that transforms an element into the
  ///   value to use for uniqueness. If `projection` returns the same value for
  ///   two different elements, the second element will be excluded from the
  ///   resulting array.
  ///
  /// - Returns: An array with only the unique elements of this sequence, as
  ///   determined by the result of `projection` for each element.
  ///
  /// - Complexity: O(*n*), where *n* is the length of the sequence.
  @inlinable
  public func uniqued<Subject: Hashable>(
    on projection: (Element) throws -> Subject
  ) rethrows -> [Element] {
    var seen: Set<Subject> = []
    var result: [Element] = []
    for element in self {
      if seen.insert(try projection(element)).inserted {
        result.append(element)
      }
    }
    return result
  }
}
