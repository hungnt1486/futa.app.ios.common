//
//  UILabel+Extension.swift
//  FutaCommon
//
//  Created by Tien Tran on 28/12/2022.
//

import UIKit

public extension UILabel {
    func countLines() -> Int {
        guard let myText = self.text as NSString? else {
            return 0
        }
        // Call self.layoutIfNeeded() if your view uses auto layout
        let rect = CGSize(width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: self.font as Any], context: nil)
        return Int(ceil(CGFloat(labelSize.height) / self.font.lineHeight))
    }
    
    func highlight(text: String?, font: UIFont? = nil, color: UIColor? = nil) {
            guard let fullText = self.text, let target = text else {
                return
            }

            let attribText = NSMutableAttributedString(string: fullText)
            let range: NSRange = attribText.mutableString.range(of: target, options: .caseInsensitive)
            
            var attributes: [NSAttributedString.Key: Any] = [:]
            if let font = font {
                attributes[.font] = font
            }
            if let color = color {
                attributes[.foregroundColor] = color
            }
            attribText.addAttributes(attributes, range: range)
            self.attributedText = attribText
        }
}

public extension UILabel {
    @IBInspectable var lineHeight: CGFloat {
        get{
            guard let text = self.text else { return 0 }
            var style: NSMutableParagraphStyle?
            attributedText?.enumerateAttribute(NSAttributedString.Key.paragraphStyle, in: NSMakeRange(0, text.count), options: NSAttributedString.EnumerationOptions(rawValue: 0), using: { (values, range, stop) in
                style =  values as? NSMutableParagraphStyle
            })
            return style?.lineSpacing ?? 0
        }
        
        set{
            let text = self.text
            if let text = text {
                var attributeString: NSMutableAttributedString?
                if  let attributedText = self.attributedText {
                     attributeString = NSMutableAttributedString(attributedString: attributedText)
                } else {
                    attributeString = NSMutableAttributedString(string: text)
                }
                guard let attributeStringUnwrap = attributeString else { return }

                let style = NSMutableParagraphStyle()
                style.alignment = self.textAlignment
                style.lineSpacing = newValue - font.pointSize
                if style.lineSpacing < 0 {
                    style.lineSpacing = 0
                }

                attributeStringUnwrap.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSMakeRange(0, attributeStringUnwrap.length))
                self.attributedText = attributeStringUnwrap
            }
        }
    }
    
    @IBInspectable var letterSpacing: CGFloat {
        get {
            var spacingValue: CGFloat?
            guard let text = self.text else { return 0 }
            attributedText?.enumerateAttribute(NSAttributedString.Key.kern, in: NSMakeRange(0, text.count), options: NSAttributedString.EnumerationOptions(rawValue: 0), using: { (values, range, stop) in
                spacingValue = values as? CGFloat
               
            })
            return spacingValue ?? 0
        }
        set {
            let text = self.text
            if let text = text {
                var attributeString: NSMutableAttributedString?
                if  let attributedText = self.attributedText {
                     attributeString = NSMutableAttributedString(attributedString: attributedText)
                } else {
                    attributeString = NSMutableAttributedString(string: text)
                }
                guard let attributeStringUnwrap = attributeString else { return }
                attributeStringUnwrap.addAttribute(NSAttributedString.Key.kern, value: newValue, range: NSMakeRange(0, text.count))
                    self.attributedText = attributeStringUnwrap
            }
        }
    }
}
