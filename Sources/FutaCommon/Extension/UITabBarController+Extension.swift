//
//  UITabBarController+Extension.swift
//  FutaCommon
//
//  Created by mac on 16/12/2022.
//
import RxSwift
import UIKit

public extension UITabBarController {
    
     struct Holder {
        public static var _isHiddenEvent = PublishSubject<Bool>()
    }
}

