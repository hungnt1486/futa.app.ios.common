//
//  UIView+Extension.swift
//  FaceCar
//
//  Created by Dung Vu on 9/27/18.
//  Copyright © 2018 Vato. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        set(radius) {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = radius > 0
        }

        get {
            return self.layer.cornerRadius
        }
    }

    @IBInspectable
    var borderWidth: CGFloat {
        set(borderWidth) {
            self.layer.borderWidth = borderWidth
        }

        get {
            return self.layer.borderWidth
        }
    }

    @IBInspectable
    var borderColor: UIColor? {
        set(color) {
            self.layer.borderColor = color?.cgColor
        }

        get {
            if let color = self.layer.borderColor {
                return UIColor(cgColor: color)
            } else {
                return nil
            }
        }
    }

    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }

    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }

    func dropShadow(radius: CGFloat = 14) {
        layer.masksToBounds = false
        shadowColor = .black
        shadowOpacity = 0.4
        shadowRadius = radius
        shadowOffset = CGSize(width: 0.0, height: 4.0)
    }
    
    func dropShadow(radius: CGFloat = 14, opacity: Float, heightOffset: Double) {
        layer.masksToBounds = false
        shadowColor = .black
        shadowOpacity = opacity
        shadowRadius = radius
        shadowOffset = CGSize(width: 0.0, height: heightOffset)
    }
    
    func roundCorners(corners: CACornerMask, radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = corners
    }
    
     func addDashedBorder(strokeColorString: String = "#C9CED1") {
         if let subLayers = self.layer.sublayers {
             for item in subLayers {
                 if item.name == "addDashedBorder" {
                     return
                 }
             }
         }
         
        let color = UIColor.init(hexString: strokeColorString).cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
         shapeLayer.name = "addDashedBorder"
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        shapeLayer.frame = shapeRect
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [5,5]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 8).cgPath
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawDotLine(strokeColorString: String = "#C9CED1") {
        if let subLayers = self.layer.sublayers {
            for item in subLayers {
                if item.name == "dotLine" {
                    return
                }
            }
        }
        
       let color = UIColor.init(hexString: strokeColorString).cgColor
       let shapeLayer:CAShapeLayer = CAShapeLayer()
        shapeLayer.name = "dotLine"
       let frameSize = self.frame.size

       shapeLayer.strokeColor = color
       shapeLayer.lineWidth = 2
       shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern =  [0.25, 4]
        
        shapeLayer.lineCap = .round
     
        
        let path = UIBezierPath()
           path.move(to: CGPoint(x: frameSize.width / 2, y: 0))
           path.addLine(to: CGPoint(x: frameSize.width / 2, y: frameSize.height))
        shapeLayer.path = path.cgPath
        
       self.layer.addSublayer(shapeLayer)
    }
    
    func isExistedLayerName(name: String ) -> Bool {
        if let subLayers = self.layer.sublayers {
            for item in subLayers {
                if item.name == name {
                    return true
                }
            }
            return false
        }
        return false
    }
    
    func addBorder(top: CGFloat, left: CGFloat, right: CGFloat, bottom: CGFloat, color: UIColor) {
        
        if bottom > 0, isExistedLayerName(name: "bottomLine") == false {
            let bottomLine = CALayer()
            bottomLine.name = "bottomLine"
            bottomLine.frame = CGRect(x: 0.0, y: self.frame.height - bottom, width: self.frame.width, height: bottom)
            bottomLine.backgroundColor = color.cgColor
            self.layer.addSublayer(bottomLine)
        }
        
        if top > 0, isExistedLayerName(name: "topLine") == false {
            let topLine = CALayer()
            topLine.name = "topLine"
            topLine.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: top)
            topLine.backgroundColor = color.cgColor
            self.layer.addSublayer(topLine)
        }
        
        if right > 0, isExistedLayerName(name: "rightLine") == false {
            let rightLine = CALayer()
            rightLine.name = "rightLine"
            rightLine.frame = CGRect(x: self.frame.width, y: 0.0, width: right, height: self.frame.height)
            rightLine.backgroundColor = color.cgColor
            self.layer.addSublayer(rightLine)
        }
        
        if left > 0, isExistedLayerName(name: "leftLine") == false{
            let leftLine = CALayer()
            leftLine.name = "leftLine"
            leftLine.frame = CGRect(x: 0.0, y: 0.0, width: left, height: self.frame.height)
            leftLine.backgroundColor = color.cgColor
            self.layer.addSublayer(leftLine)
        }
    }
    
    @discardableResult
    func addBorders(edges: UIRectEdge,
                    color: UIColor,
                    inset: CGFloat = 0.0,
                    thickness: CGFloat = 1.0) -> [UIView] {

        var borders = [UIView]()

        @discardableResult
        func addBorder(formats: String...) -> UIView {
            let border = UIView(frame: .zero)
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            border.layer.cornerRadius = 8
            addSubview(border)
            addConstraints(formats.flatMap {
                NSLayoutConstraint.constraints(withVisualFormat: $0,
                                               options: [],
                                               metrics: ["inset": inset, "thickness": thickness],
                                               views: ["border": border]) })
            borders.append(border)
            return border
        }


        if edges.contains(.top) || edges.contains(.all) {
            addBorder(formats: "V:|-0-[border(==thickness)]", "H:|-inset-[border]-inset-|")
        }

        if edges.contains(.bottom) || edges.contains(.all) {
            addBorder(formats: "V:[border(==thickness)]-0-|", "H:|-inset-[border]-inset-|")
        }

        if edges.contains(.left) || edges.contains(.all) {
            addBorder(formats: "V:|-inset-[border]-inset-|", "H:|-0-[border(==thickness)]")
        }

        if edges.contains(.right) || edges.contains(.all) {
            addBorder(formats: "V:|-inset-[border]-inset-|", "H:[border(==thickness)]-0-|")
        }

        return borders
    }
        
}


//Shimmer View
public extension UIView {
  
  // ->1
  enum ShimmerDirection: Int {
    case topToBottom = 0
    case bottomToTop
    case leftToRight
    case rightToLeft
  }
    
  func startShimmeringAnimation(animationSpeed: Float = 1.4,
                                direction: ShimmerDirection = .leftToRight,
                                repeatCount: Float = MAXFLOAT) {
      if self.viewWithTag(1111) != nil {
          return
      }
    // Create color  ->2
    let lightColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.1).cgColor
    let blackColor = UIColor.black.cgColor
    
    // Create a CAGradientLayer  ->3
      let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [blackColor, lightColor, blackColor]
    gradientLayer.frame = CGRect(x: -self.bounds.size.width, y: -self.bounds.size.height, width: 3 * self.bounds.size.width, height: 3 * self.bounds.size.height)
    
    switch direction {
    case .topToBottom:
      gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
      gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
      
    case .bottomToTop:
      gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
      gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
      
    case .leftToRight:
      gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
      gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
      
    case .rightToLeft:
      gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
      gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
    }
    
      gradientLayer.locations =  [0.35, 0.50, 0.65] //[0.4, 0.6]
      
      // add backbround color
      let bgColor =  UIColor(hexString: "#EBEDEE")
      if self.viewWithTag(1111) == nil {
          let bgView = UIView(frame: CGRect(origin: .zero, size: self.bounds.size))
          bgView.cornerRadius = self.cornerRadius
          bgView.clipsToBounds = self.clipsToBounds
          bgView.backgroundColor = bgColor
          bgView.tag = 1111
          
          let label = UILabel(frame: CGRect.zero)
          label.tag = 1112
          bgView.addSubview(label)
          
          self.addSubview(bgView)
          if let labelType = self as? UILabel {
              label.textColor = labelType.textColor
              labelType.textColor = UIColor.clear
          }
          
          if let buttonType = self as? UIButton {
              label.textColor = buttonType.titleColor(for: .normal)
              buttonType.setTitleColor(UIColor.clear, for: .normal)
          }
      }
      self.layer.mask = gradientLayer
      
    
    // Add animation over gradient Layer  ->4
    CATransaction.begin()
    let animation = CABasicAnimation(keyPath: "locations")
    animation.fromValue = [0.0, 0.1, 0.2]
    animation.toValue = [0.8, 0.9, 1.0]
    animation.duration = CFTimeInterval(animationSpeed)
    animation.repeatCount = repeatCount
    CATransaction.setCompletionBlock { [weak self] in
      guard let strongSelf = self else { return }
        strongSelf.removeBackgroudColorView()
        strongSelf.layer.mask = nil
    }
    gradientLayer.add(animation, forKey: "shimmerAnimation")
    CATransaction.commit()
  }
  
     func stopShimmeringAnimation() {
      self.removeBackgroudColorView()
      self.layer.mask = nil

  }
    
    private func removeBackgroudColorView() {
        if let bgView = self.viewWithTag(1111), let tempView = bgView.viewWithTag(1112) as? UILabel  {
            if let labelType = self as? UILabel {
                labelType.textColor = tempView.textColor
            }
            
            if let buttonType = self as? UIButton {
                buttonType.setTitleColor(tempView.textColor, for: .normal)
            }
            bgView.removeFromSuperview()
        }
    }
  
}
public extension UIView {
    func findFirstResponder() -> UIView? {
        /* Condition validation */
        guard !isFirstResponder else {
            return self
        }

        // Find and resign first responder
        for view in self.subviews {
            if view.isFirstResponder {
                return view
            } else {
                guard let nView = view.findFirstResponder() else {
                    continue
                }
                return nView
            }
        }
        return nil
    }
    
    func shake(count: Float = 2, for duration: TimeInterval = 0.2, withTranslation translation: Float = 5) {
        layer.removeAllAnimations()
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration / TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.values = [translation, -translation]
        layer.add(animation, forKey: "shake")
    }
}

