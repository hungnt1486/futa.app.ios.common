//
//  Extension+Date.swift
//  VietnamBookingIOS
//
//  Created by MACBOOK PRO on 3/31/20.
//  Copyright © 2020 VietnamBookingCorp. All rights reserved.
//

import UIKit

public extension Date {
    
    
    /// Get information from date
    ///
    /// - Parameter date: Date
    /// - Returns: Components of information.
    func getCompenent(date: Date) -> DateComponents {
        let calendar = Calendar(identifier: .gregorian)
        return calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
    }
    
    // format date to string with formatter "dd/MM/YYYY"
    /// Format date to string
    ///
    /// - Returns: String of date
    func stringValue() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = DATE_FORMAT
        return formatter.string(from: self)
    }
    
    func stringValue(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func stringValue(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    /// Number month of yead
    ///
    /// - Returns: Number month of year
    func numberMonthOfYear() -> Int {
        let dateComponents = DateComponents(year: self.year(), month: self.month())
        let calendar = Calendar(identifier: .gregorian)
        let date = calendar.date(from: dateComponents)!
        let range = calendar.range(of: .month, in: .year, for: date)!
        return range.count
    }
    
    /// Get year
    ///
    /// - Returns: year
    func year() -> Int {
        return getCompenent(date: self).year!
    }
    
    /// Get month
    ///
    /// - Returns: Month
    func month() -> Int {
        return getCompenent(date: self).month!
    }
    
    /// Get day
    ///
    /// - Returns: Day
    func day() -> Int {
        return getCompenent(date: self).day!
    }
    
    /// Get hour
    ///
    /// - Returns: Hour
    func hour() -> Int {
        return getCompenent(date: self).hour!
    }
    
    /// Get minute
    ///
    /// - Returns: Minute
    func minute() -> Int {
        return getCompenent(date: self).minute!
    }
    
    /// Compate 2 dates
    ///
    /// - Parameter date: Date to compare
    /// - Returns: True if greater than
    func isGreatThan(date: Date) -> Bool {
        if self.compare(date) == ComparisonResult.orderedAscending {
            return false
        }
        else {
            return true
        }
    }
    
    /// Compate 2 dates
    ///
    /// - Parameter date: Date to compare
    /// - Returns: true if less than
    func isLessThan(date: Date) -> Bool {
        return !isGreatThan(date: date)
    }
    
    /// Check valid date
    ///
    /// - Returns: true if the date is valid
    func isValidateDate() -> Bool {
        var out = true
        let timeInterval = self.timeIntervalSince1970
        if timeInterval < 0 {
            out = false
        }
        return out
    }
    
    /// Get unix time stanmp
    ///
    /// - Returns: unix time stamp
    func intValue() -> Int {
        return Int(self.timeIntervalSince1970)
    }
    
    
    func addingDate(amount: Int) -> String {
        
        let date = Calendar.current.date(byAdding: .day, value: amount, to: self)?.stringValue()
        
        return date ?? ""
        
    }
    
    func addingYear(amount: Int) -> String {
        
        let year = Calendar.current.date(byAdding: .year, value: amount, to: self)?.stringValue()
        
        return year ?? ""
        
    }
    
    
    func addingTime(amount: Int, format: String) -> String {
        
        let date = Calendar.current.date(byAdding: .second, value: amount, to: self)?.stringValue(format: format)
        
        return date ?? ""
        
    }
    
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    
    var calendarDay: Int {
        let components = self.get(.day, .month, .year)
        if let day = components.day {
            return day
        } else {
            return 0
        }
    }
    
    var calendarMonth: Int {
        let components = self.get(.day, .month, .year)
        if let month = components.month {
            return month
        } else {
            return 0
        }
    }
    
    var calendarYear: Int {
        let components = self.get(.day, .month, .year)
        if let year = components.year {
            return year
        } else {
            return 0
        }
    }
    
    var monthFullString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
    
    func from(day: Int = 1, month: Int, year: Int) -> Date? {
        let calendar = Calendar(identifier: .gregorian)
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour()
        dateComponents.minute = minute()
        return calendar.date(from: dateComponents) ?? Date()
    }
    
    var currentMillisecondsSince1970: Double {
        Double((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    static func dateUTC(from str: String?, format: String, identifier: String = "UTC") -> Date? {
        guard let str = str, !str.isEmpty else {
            return nil
        }
        precondition(!format.isEmpty, "Format is not empty.")
        let formater = DateFormatter()
        formater.dateFormat = format
        formater.timeZone = TimeZone(identifier: identifier)
        let result = formater.date(from: str)
        return result
    }
    
}

