//
//  UICollectionView+Extension.swift
//  FutaCommon
//
//  Created by mac on 08/08/2022.
//

import UIKit

public extension UICollectionView {
    func register(withNib nibName: String, cellIdentifider: String) {
        self.register( UINib(nibName:nibName, bundle: nil), forCellWithReuseIdentifier: cellIdentifider)
       
    }
}

public extension Collection {
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
