//
//  Data+Extension.swift
//  FutaCommon
//
//  Created by mac on 25/08/2022.
//
import Foundation
public extension Data {
    func toHexString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}

