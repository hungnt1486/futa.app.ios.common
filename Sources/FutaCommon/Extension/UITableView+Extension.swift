//
//  UITableView+Extension.swift
//  FutaCommon
//
//  Created by mac on 08/08/2022.
//

import UIKit

public extension UITableView {
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0.3, animations: { self.reloadData() })
                { _ in completion() }
    }
    
    /// Get indetifiercell
    ///
    /// - Parameter identifier: Identify string
    /// - Returns: Table cell
    func getIdentiferCell(_ identifier: String) -> UITableViewCell {
        return  dequeueReusableCell(withIdentifier: identifier)!
    }
}
