//
//  Localization.swift
//  FutaCommon
//
//  Created by mac on 23/12/2022.
//
import UIKit

public extension String {
    /* Localize with default device locale */
    func localized(_ locale: String = UserDefaults.standard.string(forKey: "app_lang") ?? "en") -> String {
        var localeId = String(locale.prefix(2))
        if !Locale.supportedLanguages.contains(localeId) {
            localeId = Locale.backendDefault.shortIdentifier
        }
        
        //        guard let path = Bundle.main.path(forResource: localeId, ofType: "lproj"),
        //              let bundle = Bundle(path: path) else {
        //            return NSLocalizedString(self, comment: "")
        //        }
        /*
         guard let bundle = Bundle(identifier: "FutaCommon.framework") else {
         return NSLocalizedString(self, comment: "")
         }
         return bundle.localizedString(forKey: self, value: nil, table: nil)
         */
        
        let bundleIdentifiers = [
//            "FutaCommon.framework",
//            "FutaCoreAPI.framework",
            "FutaPay.framework",
            "FutaExpress.framework",
            "FutaCityBus.framework",
            "FutaShuttle.framework"
        ]
        
        for identifier in bundleIdentifiers {
            if let path = Bundle(identifier: identifier)?.path(forResource: localeId, ofType: "lproj"),
               let bundle = Bundle(path: path) {
                let localizedString = bundle.localizedString(forKey: self, value: nil, table: nil)
                if localizedString != self {
                    return localizedString
                }
            }
        }
        
        guard let path = Bundle.main.path(forResource: localeId, ofType: "lproj"),
              let bundle = Bundle(path: path) else {
            return NSLocalizedString(self, comment: "")
        }
        
        return bundle.localizedString(forKey: self, value: nil, table: nil)
    }
    
}

public extension UILabel {
    @IBInspectable var localizedKey: String {
        get {
            return self.text ?? ""
        }
        set(value) {
            self.text = value.localized()
        }
    }
}

public extension UITextField {
    @IBInspectable var localizedKey: String {
        get {
            return self.placeholder ?? .init()
        }
        set(value) {
            self.placeholder = value.localized()
        }
    }
}

public extension UIButton {
    @IBInspectable var localizedKey: String {
        get {
            return self.titleLabel?.text ?? .init()
        }
        set(value) {
            self.setTitle(value.localized(), for: .normal)
        }
    }
}


public extension Locale {
    static var backendDefault: Locale {
        return Locale(identifier: "en")
    }
    
    static var supportedLanguages: [String] {
       // return Bundle.getArrayInfoFor(key: AppEnvironment.infoPlist.supportedLanguages.rawValue)
        return ["vi", "en"]
    }
    
    var shortIdentifier: String {
        return String(identifier.prefix(2))
    }
    
    var isEnglish: Bool {
        let locale: String = Locale.preferredLanguages[0]
        var localeId = String(locale.prefix(2))
        if !Locale.supportedLanguages.contains(localeId) {
            localeId = Locale.backendDefault.shortIdentifier
        }
        
        return localeId == "en"
    }
    
}
