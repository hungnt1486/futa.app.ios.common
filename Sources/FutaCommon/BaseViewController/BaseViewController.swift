//
//  BaseViewController.swift
//  FUTACustomer
//
//  Created by Duong Nguyen on 15/11/2021.
//  Copyright © 2021 FUTA Group. All rights reserved.
//

import Foundation
import UIKit
import UserNotificationsUI
import FutaCoreAPI
import RxSwift

open class BaseViewController: UIViewController {
    public let disposeBag = DisposeBag()
    public var isHiddenNavigationBar: Bool?
    public var isAnimatedHiddenNavi: Bool = false
    
    fileprivate var viewTranslation = CGPoint(x: 0, y: 0)
    private lazy var loadingAnimation: LoadingAnimation = {
        return LoadingAnimation(frame: CGRect(x: 0, y: 0, width: 220, height: 87))
    }()
    
    private lazy var cireLoadingView: CircleLoadingView = {
       return CircleLoadingView(frame: CGRect(x: 0, y: 0, width: 90, height: 90))
    }()
    
    private lazy var loadingInLineView: LoadingInLineView = {
        return LoadingInLineView()
    }()
        
    private var errorFullScreen: ErrorFullScreenView?
    private var emptyDataFullScreen: EmptyDataFullScreen?
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    open override var hidesBottomBarWhenPushed: Bool {
        get {
            UITabBarController.Holder._isHiddenEvent.onNext(super.hidesBottomBarWhenPushed)
            return super.hidesBottomBarWhenPushed
        }
        
        set(newValue) {
            super.hidesBottomBarWhenPushed = newValue
            UITabBarController.Holder._isHiddenEvent.onNext(newValue)
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let isHiddenNavigationBar  = isHiddenNavigationBar {
            navigationController?.setNavigationBarHidden(isHiddenNavigationBar, animated: isAnimatedHiddenNavi)
        }
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let isHiddenNavigationBar  = isHiddenNavigationBar {
            navigationController?.setNavigationBarHidden(!isHiddenNavigationBar, animated: isAnimatedHiddenNavi)
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        addingTapGuesture()
        setupNavigationSwipeBack()
    }
    
    /// Show error alert when call API error
    /// - Parameter error: error from server
    func showError(errorMessage:  String) {
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func getImageMarkerUserOnline() -> UIImage? {
        let markerImage = UIImage(named: "iconBookingMarkerUserOnline")
        return markerImage
    }
    
    func getImageMarkerSelected() -> UIImage? {
        let markerImage = UIImage(named: "iconBookingMarkerBus")
        return markerImage
    }
    
    func getImageMarkerUnSelected() -> UIImage? {
        let markerImage = UIImage(named: "iconBookingMarkerBusStation")
        return markerImage
    }
    
    
    private func setupNavigationSwipeBack() {
           //delegateSwipeBack
           self.navigationController?.interactivePopGestureRecognizer?.delegate = self
           // enable/disable swipeback
           if let count =  self.navigationController?.viewControllers.count, count > 1 {
               self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
           } else {
               self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
           }
       }
    
    // Hung add
    
    public func showEmptyDataFullScreen(should: Bool = true,
                                    message: String = "no_data_found".localized(),
                                    inView: UIView) {
        if should {
            emptyDataFullScreen = EmptyDataFullScreen(message: message)
            emptyDataFullScreen?.showInView(view: inView)
        } else {
            emptyDataFullScreen?.hide()
        }
    }
    
    public func showErrorFullScreen(should: Bool = true,
                                    message: String = "error_occurred_try_again_later".localized(),
                                    inView: UIView, retryCallback: (() -> ())? = nil) {
        if should {
            errorFullScreen = ErrorFullScreenView(message: message, retryCallback: retryCallback)
            errorFullScreen?.showInView(view: inView)
        } else {
            errorFullScreen?.hide()
        }
    }
    
    
    public func shouldShowLoadingInline(should: Bool, inView: UIView, userInteraction: Bool = true) {
        loadingInLineView.shouldShowLoading(should: should, view: inView, isUserInteractionEnabled: userInteraction)
    }
    
    public func shouldShowLoading(should: Bool, userInteraction: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if should {
                self.cireLoadingView.show(viewController: self, userInteraction: userInteraction)
            } else {
                self.cireLoadingView.hide()
                
            }
        }
    }
    
    public func shouldShowLoadingFullscreen(should: Bool, contenView: UIView? = nil, userInteraction: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            contenView?.isHidden = should
            if should {
                self.loadingAnimation.show(viewController: self, userInteraction: userInteraction)
            } else {
                self.loadingAnimation.hide()
            }
        }
    }
    
    public func showErrorDialog(error: FutaAPIError) {
        switch error {
        case .networkDown:
            MessageAlert.showNetworkDown()
            debugPrint("== networkDown")
         default:
            debugPrint("== default")
            MessageAlert.showError(message: error.localizedDescription)
        }
    }
    
    public func showMessage(title: String = "notification".localized(),
                            messsage: String,
                            cancelTitle: String = "close".localized(),
                            cancelCallback: (() -> ())? = nil) {
        MessageAlert.showMessage(title: title, message: messsage, cancelTitle: cancelTitle, cancelCallback: cancelCallback)
    }
    
    public func showConfirmPopup(title: String = "notification".localized(),
                                   message: String,
                                   cancelTitle: String = "cancel".localized(),
                                   okTitle: String = "ok".localized(),
                                   cancelCallback: @escaping (() -> ()),
                                   okCallback: @escaping (() -> ()) ) {
        MessageAlert.showConfirmMessage(title: title, message: message, cancelTitle: cancelTitle, okTitle: okTitle, cancelCallback: cancelCallback, okCallback: okCallback)
        
    }
    
    public func showSucessPopup(title: String?, message: String, hideComplete: (() -> ())? = nil) {
        let popupSuccess = PopupSuccess(title: title, message: message)
        popupSuccess.callBack = {
            hideComplete?()
        }
        popupSuccess.show()
    }
    
    public func showFailPopup(title: String?, error: FutaAPIError) {
        switch error {
        case .networkDown:
            MessageAlert.showNetworkDown()
            debugPrint("== networkDown")
         default:
            debugPrint("== default")
            let popFail = PopupFail(title: title, message: error.localizedDescription)
            popFail.show()
        }
    }
    
    public func showFailPopupMessage(title: String?, errorMessage: String, hideComplete: (() -> ())? = nil) {
        let popFail = PopupFail(title: title, message: errorMessage)
        popFail.callBack = {
            hideComplete?()
        }
        popFail.show()
    }
    
    func viewWillSwipeBack() {
        
    }
    // end Hung
    
}


/*--------------------------
 MARK:- SET UP MapView
 ---------------------------*/


/*--------------------------
 MARK:- SET UP Notification
 ---------------------------*/
extension BaseViewController {
    func notify(message: String) {
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = "Thông báo"
        content.body = message
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "message.wav"))
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "BaseViewController", content: content, trigger: trigger)
        center.add(request) { (e) in
            guard let e = e else { return }
            assert(false, e.localizedDescription)
        }
    }
}


/*--------------------------
 MARK:- Helper Methods Notification, Adding Tap Guestur
 ---------------------------*/
 extension BaseViewController {
    
    
    
    func addTextFieldDidChange(textFields:UITextField...){
        for textField in textFields{
            textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        //let t = textField.tag
        
    }
    internal func addTapForGesture(views:UIView...){
        for view in views{
            addTapGesture(view: view, selector: #selector(onClick(sender:)))
        }
    }
    
    /* GESTURE */
    internal func addTapGesture(view: UIView, selector: Selector?){
        
        let tap = UITapGestureRecognizer(target: self, action: selector)
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
    }
    
    @objc internal func onClick(sender: UITapGestureRecognizer) {
        
    }
    
    
    /// Adding Tap Guesture
    func addingTapGuesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.delegate = self
        
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
//    /// Delegate for gesture dismiss keyboard when touch to view
//    @objc(gestureRecognizer:shouldReceiveTouch:) func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if touch.view is UIControl {
//            return false
//        }
//        return true
//    }
    
    
    /// Adding Keyboard Notification Observer
   public func addKeyboardShowNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /// Function called when keyboard will show
    ///
    /// - Parameter notification: information from keyboard
     @objc open func keyboardWillShow(notification: NSNotification) {
        
    }
    
    
    /// Function called when keyboard will hide
    ///
    /// - Parameter notification: information from keyboard
     @objc open func keyboardWillHide(notification: NSNotification)
    {
        
    }
    
    /// Dissmiss Keyboard
    @objc open func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    func addingSwipeGestureDown() {
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            viewTranslation = sender.translation(in: view)
            if viewTranslation.y < 0 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = .identity
                })
            } else {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                })
            }
        case .ended:
            if viewTranslation.y < 200 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = .identity
                })
            } else {
                dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
        
        
    }
}

extension BaseViewController: UIGestureRecognizerDelegate {
    open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIControl {
            return false
        }
        
        return true
    }
    
    open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            self.viewWillSwipeBack()
        }
        return true
    }
}
