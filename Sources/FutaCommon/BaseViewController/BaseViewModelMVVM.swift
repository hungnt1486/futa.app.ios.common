//
//  BaseViewModel.swift
//  FUTACustomer
//
//  Created by mac on 26/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import RxSwift
import Foundation
import RxRelay
import FutaCoreAPI

open class BaseViewModelMVVM {
    public let disposeBag = DisposeBag()
    public let loadingEvent = PublishSubject<Bool>()
    public let loadingFullScreenEvent = PublishSubject<Bool>()
    public let emptyDataEvent = PublishSubject<Void>()
    public let errorEvent = PublishSubject<FutaAPIError>()
    public let errorFullscreenEvent = PublishSubject<FutaAPIError>()
    
    public init() {
        
    }
}
