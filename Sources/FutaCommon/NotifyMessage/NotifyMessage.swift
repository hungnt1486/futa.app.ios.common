//
//  NotifyMessage.swift
//  FutaCommon
//
//  Created by mac on 30/08/2022.
//

import UIKit

public enum NotifyMessageType {
    case info
    case warning
    case error
    
    func backgroundColor() -> UIColor {
        switch self {
        case .info:
            return UIColor(hexString: "#709E35")
        case .warning:
            return UIColor(hexString: "#E7BE49")
        case .error:
            return UIColor(hexString: "#E7513D")
        }
    }
}

public class NotifyMessage: UIView {
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    
    let timeAutohide: TimeInterval = 3.0
    
    private var title: String?
    private var message: String
    private var notifyMessageType: NotifyMessageType
    
    public init(title: String?, message: String, notifyMessageType: NotifyMessageType = .info) {
        self.title = title
        self.message = message
        self.notifyMessageType = notifyMessageType
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 92))
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.message = ""
        self.notifyMessageType = .info
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public func setTitle(title: String) {
        self.title = title
    }
    
    func setMessage(message: String) {
        self.message = message
    }
    
    public func show(inViewController: UIViewController? = nil) {
        let currentWindow = UIApplication.shared.windows.first { $0.isKeyWindow }
        if let viewController = inViewController, let topViewController = UIViewController.topMostViewController()  {
            if  viewController == topViewController {
                currentWindow?.addSubview(self)
            }
        } else {
            currentWindow?.addSubview(self)
        }
        
        self.transform = CGAffineTransform(translationX: 0, y: -self.frame.height)
        let statusBarHeight: CGFloat = currentWindow?.safeAreaInsets.top ?? 0
        
        UIView.transition(with: self, duration: 0.3, options: [.curveEaseInOut]) { [weak self] in
            guard let self = self else { return }
            self.transform = CGAffineTransform(translationX: 0, y: statusBarHeight)
            
        } completion: { [weak self] finish in
            guard let self = self else { return }
            self.perform(#selector(self.hide), with: nil, afterDelay: self.timeAutohide, inModes: [RunLoop.Mode.common])
        }
        
    }
    
    
    @objc func hide() {
        UIView.transition(with: self, duration: 0.3, options: [.curveEaseInOut]) { [weak self] in
            guard let self = self else { return }
            self.transform = CGAffineTransform(translationX: 0, y: -self.frame.height)
            
        } completion: { finish in
            self.removeFromSuperview()
            self.transform = CGAffineTransform.identity
        }
    }
    
    //MARK: private
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed("NotifyMessage", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
        configUI()
    }
    
    private func configUI() {
        titleLabel.isHidden = title == nil
        titleLabel.text = title
        messageLabel.text = message
        
        containerView.backgroundColor = notifyMessageType.backgroundColor()
        containerView.clipsToBounds = false
        containerView.dropShadow()
        
        let tapBackground = UITapGestureRecognizer(target: self, action: #selector(tapBackground))
        containerView.addGestureRecognizer(tapBackground)
    }
    
    @objc private func tapBackground() {
        hide()
    }
}
