//
//  FutaSearchBar.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 16/05/2023.
//

import UIKit

@IBDesignable public class FutaSearchBar: UIView {
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var bgView: UIView!
    @IBOutlet private(set) weak var textField: UITextField!
    
    private let placeHolderColor = UIColor(hexString: "#A2ABB3")
    
    public var didTextChangeCallback: ((String?) -> Void)?
    

    @IBInspectable
    public var localizedKey: String? {
           set {
               textField.placeholder = newValue?.localized()
           }
           get {
               return textField.placeholder
           }
       }
    
    @IBInspectable
    public  var bgColor: UIColor? {
           set {
               contentView.backgroundColor = .clear
               bgView.backgroundColor = newValue
           }
           get {
               return bgView.backgroundColor
           }
       }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        commonInit()
    }
    
    
    //MARK: Private
    private func commonInit() {
        FutaCommonConstant.bundle.loadNibNamed("FutaSearchBar", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
        configUI()
    }
    
    private func configUI() {
       // textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.attributedPlaceholder =
        NSAttributedString(string: "placeholder text", attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor])
    }
    
    @objc private func textFieldDidChange(tf: UITextField) {
        didTextChangeCallback?(tf.text)
    }
    
}
