//
//  PopLocationToZoneViewModel.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import FutaCoreAPI
import Action
import RxSwift

class PopupListProvinceViewModel: BaseViewModelMVVM {
    
    private let useCase = GetListProvinceUseCaseImpl()
    private lazy var getListProvinceAction = makeGetListPorvinceAction()
    
    let getProvinceSuccessEvent = PublishSubject<Void>()
    let reloadDataEvent = PublishSubject<Void>()
    
    private var listProvince: [ProvinceModel] = []
    private(set) var searchResultProvince: [ProvinceModel] = []

    private(set) var currentSelectedProvince: ProvinceModel?
    
    init(currentSelectedProvince: ProvinceModel? = nil) {
        self.currentSelectedProvince = currentSelectedProvince
        super.init()
        configGetListProvince()
    }
    
    //MARK: Public
    func viewDidload() {
        getListProvinceAction.execute()
    }
    
    func handleSelectProvince(province: ProvinceModel) {
        currentSelectedProvince = province
        reloadDataEvent.onNext(())
    }
    
    func searchText(text: String) {
        if text.isEmpty {
            searchResultProvince = listProvince
        } else {
            searchResultProvince.removeAll()
            for item in listProvince {
                let found = item.name.search(text)
                if found {
                    searchResultProvince.append(item)
                }
            }
        }
        
        reloadDataEvent.onNext(())
        
    }
    
    private func makeGetListPorvinceAction() -> Action<Void, [ProvinceModel]> {
        return Action<Void, [ProvinceModel]> { [weak self] in
            guard let self = self else { return Observable.empty()}
            return self.useCase.getListProvince()
        }
    }
    
    private func configGetListProvince() {
        getListProvinceAction
            .elements
            .subscribe(onNext: { [weak self] data in
                self?.listProvince = data
                self?.searchResultProvince = data
                self?.getProvinceSuccessEvent.onNext(())
            }).disposed(by: disposeBag)
        
        getListProvinceAction
            .executing
            .subscribe(onNext: { [weak self] loading in
                self?.loadingFullScreenEvent.onNext(loading)
            }).disposed(by: disposeBag)
        
        getListProvinceAction
            .errors
            .apiError
            .subscribe(onNext: { [weak self] error in
                self?.errorFullscreenEvent.onNext(error)
            }).disposed(by: disposeBag)
        
    }
}
