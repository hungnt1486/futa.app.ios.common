//
//  ProvinceCell.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import UIKit

class ProvinceCell: UITableViewCell {
    
    @IBOutlet private weak var checkImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!

  
    func bindData(name: String, isChecked: Bool) {
        selectionStyle = .none
        nameLabel.text = name
        
        let image = isChecked ? UIImage(named: "ic_radio_selection", in: FutaCommonConstant.bundle, with: nil) : UIImage(named: "ic_unchecked_radio", in: FutaCommonConstant.bundle, with: nil)
        checkImageView.image = image
    }
    
}
