//
//  PopupListProvince.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 06/06/2023.
//

import UIKit
import RxSwift

public class PopupListProvince: BasePopupViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var searchBar: FutaSearchBar!
    
    private let viewModel: PopupListProvinceViewModel
    private let cellIdentifier = "ProvinceCell"
    public var selectedProvinceCallback:((ProvinceModel) -> Void)?
    
    public init(currentSelectedProvince: ProvinceModel? = nil) {
        self.viewModel = PopupListProvinceViewModel(currentSelectedProvince: currentSelectedProvince)
        super.init(nibName: "PopupListProvince", bundle: FutaCommonConstant.bundle)
        self.popUpTranstion = .fromBottom
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        bindEvent()
        configUI()
        viewModel.viewDidload()
        
    }
    
    //MARK: private
    private func configUI() {
        tableView.register(UINib(nibName: cellIdentifier, bundle: FutaCommonConstant.bundle),
                           forCellReuseIdentifier: cellIdentifier)
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    private func bindEvent() {
        
        searchBar.didTextChangeCallback = { [weak self] text in
            self?.viewModel.searchText(text: text ?? "")
        }
        
        viewModel
            .getProvinceSuccessEvent
            .subscribe(onNext: { [weak self] in
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        viewModel
            .reloadDataEvent
            .subscribe(onNext: { [weak self] in
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        viewModel
            .errorFullscreenEvent
            .subscribe(onNext: { [weak self] error in
                guard let self =  self else { return }
                self.showErrorFullScreen(message: error.localizedDescription, inView: self.contentView) {
                    self.viewModel.viewDidload()
                }
            }).disposed(by: disposeBag)
        
        viewModel
            .loadingFullScreenEvent
            .subscribe(onNext: { [weak self] loading in
                guard let self =  self else { return }
                self.shouldShowLoadingFullscreen(should: loading, contenView: self.contentView)
            }).disposed(by: disposeBag)
    }
    
    @objc private func hidePopup() {
        self.hide()
        if let currentSelectItem = viewModel.currentSelectedProvince {
            selectedProvinceCallback?(currentSelectItem)
        }
    }
    
    //MARK: IBAction
    
    @IBAction func buttonCloseTouch() {
        hide()
    }
    
    //MARK:  UITableViewDelegate, UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchResultProvince.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProvinceCell {
            let item = viewModel.searchResultProvince[indexPath.row]
            var isChecked = false
            if let currentSelectedItem = viewModel.currentSelectedProvince,
               currentSelectedItem.id == item.id {
                isChecked = true
            }
            cell.bindData(name: item.name, isChecked: isChecked)
            return cell
        }
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.searchResultProvince[indexPath.row]
        viewModel.handleSelectProvince(province: item)
        perform(#selector(hidePopup), with: nil, afterDelay: 0.5)
    }
}
