//
//  ScanFailScreen.swift
//  FUTACustomer
//
//  Created by DEV on 15/07/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import UIKit

public class ErrorFullScreenView: UIView {
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var retryButton: FutaButton!
    
    public var retryCallback: (() -> ())?
    
    private let message: String
    
    public init(message: String, retryCallback: (() -> ())? = nil) {
        self.message = message
        self.retryCallback = retryCallback
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 102))
        commonInit()
    }
    
    public override init(frame: CGRect) {
        message = "error_occurred_try_again_later".localized()
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
         message = "error_occurred_try_again_later".localized()
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public func showInView(view: UIView) {
        self.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.backgroundColor = view.backgroundColor
        view.addSubview(self)
    }
    
    public func hide() {
        self.removeFromSuperview()
    }
    
    //MARK: private
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed("ErrorFullScreenView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
        configUI()
    }
    
    private func configUI() {
        messageLabel.text = message
        if retryCallback == nil {
            retryButton.isHidden = true
        }
    }
    
    //MARK: IBAction
    @IBAction func buttonRetryTouch() {
        hide()
        retryCallback?()
    }
    
}
