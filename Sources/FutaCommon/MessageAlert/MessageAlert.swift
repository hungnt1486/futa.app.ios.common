//
//  MessageAlert.swift
//  FutaCommon
//
//  Created by mac on 22/08/2022.
//

import UIKit

public class MessageAlert: BasePopupViewController {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var buttonOK: UIButton!
    @IBOutlet private weak var buttonCancel: UIButton!
    @IBOutlet private weak var hLineSpacing: UIView!
    @IBOutlet private weak var stackview: UIStackView!
   
    public var cancelCallback: (() -> ())?
    public var okCallback: (() -> ())?
    
    private func showAlert(title: String = "notification".localized(),
                   messsage: String,
                    cancelTitle: String = "close".localized(),
                   okTitle: String? = nil) {
        
        
        titleLabel.text = title
        messageLabel.text = messsage
        buttonCancel.setTitle(cancelTitle, for: .normal)
        buttonOK.setTitle(okTitle, for: .normal)
        
        if okTitle == nil {
            buttonOK.isHidden = true
            hLineSpacing.isHidden = true
            buttonCancel.setTitleColor(UIColor(hexString: "#EF5222"), for: .normal)
        } else {
            buttonOK.setTitleColor(UIColor(hexString: "#EF5222"), for: .normal)
        }
        show()
    }
    
    static func showMessage(title: String, message: String, cancelTitle: String, cancelCallback: (() -> ())? = nil) {
        let bundle = Bundle(for:  self)
        let alert = MessageAlert(nibName: "MessageAlert", bundle: bundle)
        alert.popUpTranstion = .fromCenter
        _ = alert.view
        
        alert.cancelCallback = cancelCallback
        alert.showAlert(title: title,
                        messsage: message,
                        cancelTitle: cancelTitle,
                        okTitle: nil)
    }
    
    static func showConfirmMessage(title: String,
                                   message: String,
                                   cancelTitle: String,
                                   okTitle: String,
                                   cancelCallback: @escaping (() -> ()),
                                   okCallback: @escaping (() -> ())) {
        let bundle = Bundle(for:  self)
        let alert = MessageAlert(nibName: "MessageAlert", bundle: bundle)
        alert.popUpTranstion = .fromCenter
        _ = alert.view
        alert.cancelCallback = cancelCallback
        alert.okCallback = okCallback
        alert.showAlert(title: title,
                        messsage: message,
                        cancelTitle: cancelTitle,
                        okTitle: okTitle)
    }
    
    static func showError(message: String) {
        let bundle = Bundle(for:  self)
        let alert = MessageAlert(nibName: "MessageAlert", bundle: bundle)
        alert.popUpTranstion = .fromCenter
        _ = alert.view
        alert.showAlert(title: "error".localized(),
                        messsage: message,
                        cancelTitle: "cancel".localized(),
                        okTitle: nil)
    }
    
    static func showNetworkDown() {
        let bundle = Bundle(for:  self)
        let alert = MessageAlert(nibName: "MessageAlert", bundle: bundle)
        alert.popUpTranstion = .fromCenter
        _ = alert.view
        alert.showAlert(title: "disconnected".localized(),
                        messsage: "disconnected_content".localized(),
                        cancelTitle: "later".localized(),
                        okTitle: "agree".localized())
        alert.okCallback = {
            guard let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //MARK: IBAction
    @IBAction private func buttonCancelTouch() {
        hide({ [weak self] in
            self?.cancelCallback?()

        })
        
    }
    
    @IBAction private func buttonOKTouch() {
        hide({ [weak self] in
            self?.okCallback?()

        })
    }

}
