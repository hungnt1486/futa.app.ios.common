//
//  ImageViewerScreen.swift
//  FutaCommon
//
//  Created by Vu Mai Hoang Hai Hung on 09/03/2023.
//

import UIKit
import SnapKit
import SDWebImage

public class ImageViewerScreen: BaseViewController {
    @IBOutlet private weak var selectedImageView: UIImageView!
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var subScrollView: UIScrollView!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var imagesStackView: UIStackView!
    @IBOutlet private weak var closeButton: UIButton!
    private let images: [Any]?
    private let selectedImage: Any?
    
    private let hightLightColor = UIColor(hexString: "#F59D4B")
    private let borderWidth = 2.0
    
    public init(selectedImage: Any?, images: [Any]? = nil) {
        self.selectedImage = selectedImage
        self.images = images
        
        super.init(nibName: "ImageViewerScreen", bundle: FutaCommonConstant.bundle)
        self.isHiddenNavigationBar = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
    }
    
    //MARK: private
    
    private func configUI() {
        
        let closeImage = navigationController != nil ? UIImage(named: "ic_arrow_back_white", in: FutaCommonConstant.bundle, with: nil) : UIImage(named: "ic_close_popup_white", in: FutaCommonConstant.bundle, with: nil)
        
        closeButton.setImage(closeImage, for: . normal)
        
        if let images = images as? [UIImage], let selectedImage = selectedImage as? UIImage {
            var index = 1
            for item in images {
                let imageView = UIImageView()
                imageView.image = item
                imageView.contentMode = .scaleAspectFill
                imageView.cornerRadius = 8
                imageView.borderWidth = borderWidth
                if item == selectedImage {
                    imageView.borderColor = hightLightColor
                    countLabel.text = "\(index)/\(images.count)"
                } else {
                    imageView.borderColor = UIColor.gray
                }
                imageView.tag = index
                imageView.snp.makeConstraints { make in
                    make.height.equalTo(92)
                    make.width.equalTo(92)
                }
                imagesStackView.addArrangedSubview(imageView)
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapImage))
                imageView.addGestureRecognizer(tapGesture)
                imageView.isUserInteractionEnabled = true
                index += 1
            }
        } else if let images = images as? [String], let selectedImage = selectedImage as? String {
            var index = 1
            for item in images {
                let imageView = UIImageView()
                
                if let url = URL(string: item) {
                    imageView.sd_setImage(with: url)
                }
                
                imageView.contentMode = .scaleAspectFill
                imageView.cornerRadius = 8
                imageView.borderWidth = borderWidth
                if item == selectedImage {
                    imageView.borderColor = hightLightColor
                    countLabel.text = "\(index)/\(images.count)"
                } else {
                    imageView.borderColor = UIColor.gray
                }
                imageView.tag = index
                imageView.snp.makeConstraints { make in
                    make.height.equalTo(92)
                    make.width.equalTo(92)
                }
                imagesStackView.addArrangedSubview(imageView)
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapImage))
                imageView.addGestureRecognizer(tapGesture)
                imageView.isUserInteractionEnabled = true
                index += 1
            }
        } else {
            heightConstraint.constant = 0
        }
        
        if let selectedImage = selectedImage as? UIImage {
            selectedImageView.image = selectedImage
        } else if let selectedImage = selectedImage as? String {
            if let url = URL(string: selectedImage) {
                selectedImageView.sd_setImage(with: url)
            }
        }
        
        scrollView.maximumZoomScale = 4.0
        scrollView.minimumZoomScale = 1.0
        scrollView.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(resetZoom))
        tapGesture.numberOfTapsRequired = 2
        selectedImageView.isUserInteractionEnabled = true
        selectedImageView.addGestureRecognizer(tapGesture)
        
        countLabel.isHidden = images == nil
        
    }
    
    @objc private func tapImage(tapGesture: UITapGestureRecognizer) {
        if let imageView = tapGesture.view as? UIImageView, let images = images {
            selectedImageView.image = imageView.image
            scrollView.setZoomScale(1.0, animated: false)
            
            for itemView in imagesStackView.subviews {
                if let imageView = itemView as? UIImageView {
                    imageView.borderColor = UIColor.gray
                }
            }
            
            imageView.borderColor = hightLightColor
            countLabel.text = "\(imageView.tag)/\(images.count)"
        }
    }
    
    @objc func resetZoom() {
        scrollView.setZoomScale(1.0, animated: true)
    }
    
    //MARK: IBAction
    @IBAction func buttonBackTouch() {
        if let nav = navigationController {
            nav.popViewController(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
}

extension ImageViewerScreen: UIScrollViewDelegate {
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return selectedImageView
    }
}
