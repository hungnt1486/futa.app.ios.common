//
//  ChangePasswordDialog.swift
//  FUTACustomer
//
//  Created by DEV on 04/06/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import UIKit

public class PopupSuccess: BasePopupViewController {

    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var icImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subTitleLabel: UILabel!
    @IBOutlet private weak var closeButton: UIButton!
    private var message: String = ""
    private var popUpTitle: String?
    private var isHiddenTitle: Bool = false
    var callBack: (() -> Void)?
    
    init(title: String?, message: String) {
        let bundle = Bundle(for: PopupSuccess.self)
        super.init(nibName: "PopupSuccess", bundle: bundle)
        self.popUpTranstion = .fromCenter
        self.message = message
        self.popUpTitle = title
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func hide() {
        super.hide()
        callBack?()
    }
    
    //MARK: private
    private func setupUI() {
        self.contentView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner], radius: 20.0)
        self.subTitleLabel.text = message
        self.titleLabel.isHidden = popUpTitle == nil
        self.titleLabel.text = popUpTitle
    }
    
    
    //MARK: IBAction
    @IBAction func closeTouch(_ sender: Any) {
        self.hide()
    }
   
}
